# RADRANE

RADRANE (**R**esilient **a**d hoc **d**igital **ra**dio **n**etwork for **e**mergencies) is a communication system for use during emergencies and adverse situations, tailored for low-power embedded devices running [Mbed OS](https://os.mbed.com/mbed-os/).

It is 100% autonomous as it does not rely on cell networks nor an Internet connection. As long as there is power, RADRANE allows you to exchange text messages by using [MQTT](https://en.wikipedia.org/wiki/MQTT) over [Wi-SUN](https://wi-sun.org/), a wireless technology originally made for smart meters.

## Context

This software was developped during the last semester of Bachelor at [HEIG-VD](https://heig-vd.ch/en) (School of Engineering and Management of Yverdon-les-Bains), by [Moïn Danai](mailto:moin.danai@heig-vd.ch).

## Code structure

RADRANE is composed of three parts:

- Border Router (BR)- the master node of the network,
- Router - the routing nodes of the network,
- Flask webapp - a convenient Web application to use RADRANE's message service through a browser.

Each directory contains the corresponding application code.

### Mbed applications

**Border Router**

```
├── BSP_DISCO_F769NI                   // Board Support Package for the DISCO target
├── drivers                            // drivers for the mesh BR
├── hal_stm_lvgl                       // LVGL porting for the DISCO target
│   ├── tft                            // LCD support
│   │   ├── tft.c
│   │   └── tft.h                      // display rotation (180°) can be set here
│   └── touchpad                       // touchscreen support
│       ├── touchpad.c
│       └── touchpad.h
├── lv_conf.h                          // LVGL configuration
├── mbed_app.json                      // configuration file
├── samples                            // sample configurations and scripts for MQTT
│   ├── mosquitto.conf.v6
│   └── run_bisquitt.sh
└── source
    ├── display                        // UI code
    │   ├── display_helper.cpp
    │   ├── display_helper.h
    │   └── symbol_custom.c            // custom symbols
    ├── main.cpp
    ├── mbedtls_wisun_config.h         // config for MbedTLS
    ├── mesh                           // mesh code
    │   ├── borderrouter_helpers.c
    │   ├── borderrouter_helpers.h
    │   ├── borderrouter_init.cpp
    │   ├── borderrouter_init.h
    │   ├── borderrouter_tasklet.c
    │   ├── borderrouter_tasklet.h
    │   ├── borderrouter_ws.c
    │   ├── cfg_parser.c
    │   ├── cfg_parser.h
    │   ├── nanostack_heap_region.c
    │   ├── nanostack_heap_region.h
    │   ├── static_6lowpan_config.h
    │   ├── sync_helper.cpp            // sync logic
    │   ├── sync_helper.h
    │   ├── wisun_data.cpp             // data bridge between mesh and UI logic
    │   └── wisun_data.h
    ├── mqtt                           // MQTT code
    │   ├── mqtt_helper.cpp
    │   └── mqtt_helper.h
    ├── radrane_info.h                 // release information
    ├── time                           // time code
    │   ├── time_helper.cpp
    │   └── time_helper.h
    └── wisun_certificates_test.h      // test PKI
```

Most of the mesh logic is derived from the [nanostack-border-router](https://github.com/PelionIoT/nanostack-border-router/) example repo from Izuma Networks.

**Router**

```
├── BSP_DISCO_F769NI                   // Board Support Package for the DISCO target
├── hal_stm_lvgl                       // LVGL porting for the DISCO target
│   ├── tft                            // LCD support
│   │   ├── tft.c
│   │   └── tft.h                      // display rotation (180°) can be set here
│   └── touchpad                       // touchscreen support
│       ├── touchpad.c
│       └── touchpad.h
├── lv_conf.h                          // LVGL configuration
├── mbed_app.json                      // configuration file
├── mbedtls_wisun_config.h             // config for MbedTLS
└── source
    ├── display                        // UI code
    │   ├── display_helper.cpp
    │   ├── display_helper.h
    │   └── symbol_custom.c            // custom symbols
    ├── main.cpp
    ├── mesh                           // mesh code
    │   ├── mesh_helper.cpp
    │   ├── mesh_helper.h
    │   ├── sync_helper.cpp            // sync logic
    │   └── sync_helper.h
    ├── mqtt                           // MQTT code
    │   ├── mqtt_helper.cpp
    │   └── mqtt_helper.h
    ├── radrane_info.h                 // release information
    ├── time                           // time code
    │   ├── time_helper.cpp
    │   └── time_helper.h
    └── wisun_certificates.h           // test PKI
```

Some parts of the mesh logic are derived from the [mbed-os-example-mesh-minimal](https://github.com/ARMmbed/mbed-os-example-mesh-minimal) example repo from ARM.

### Web app

```
├── mqtt-sn-tools          // MQTT-SN client
├── radrane                // application code
│   ├── config.py          // config
│   ├── forms.py           // HTML forms
│   ├── __init__.py        // init logic
│   ├── static             // static files (Bootstrap, etc)
│   ├── templates          // HTML pages
│   └── views.py           // view logic
├── scripts                // helper scripts
│   └── test_client.py     // MQTT client to subscribe to topics
└── setup.py               // Python dependencies
```

## Requirements

### Hardware

RADRANE is currently built for a specific configuration:

- Motherboard: [DISCO-F769NI board](https://os.mbed.com/platforms/ST-Discovery-F769NI/)
- RF shield: [X-NUCLEO-S2868A2](https://os.mbed.com/components/X-NUCLEO-S2868A1/)
- RTC: [DS3231](https://os.mbed.com/components/DS3231/) (only for BR)

This is not hard requirements, merely a first implementation of the UI and time features for this target. Support for other platforms can be achieved by reusing almost all of the non-UI code but this is not the case yet.

The assembled setup looks like this:

![](img/hw_lcd.jpg)

![](img/hw_rtc.jpg)

For reference, the RTC module is a [PiRTC](https://www.adafruit.com/product/4282) that can be connected as follows (same color as cables above):

![](img/rtc_pinout.png)

### Software

The code is written for ARM Mbed; no other OS support is planned.

An MQTT broker along with an MQTT-SN gateway must be available in the uplink network of the BR. This can run on an SBC such as the Raspberry Pi. The following software have been tested and are thus recommended:

- Broker: [mosquitto](https://github.com/eclipse/mosquitto)
- Gateway: [bisquitt](https://github.com/energomonitor/bisquitt)

Sample configuration and scripts are provided under `border-router/samples`.

## Usage

RADRANE goes through a couple steps before messages can be sent:

1. The BR is started with an external RTC (currently DS3231) to store & provide time information.
2. MQTT broker information is set into the BR: IPv6 address and port number.
3. The mesh is bootstraped from the BR which gets an IPv6 prefix from its backhaul/uplink over Ethernet.
   1. Sync messages are multicasted at a fixed interval to provide information to new nodes, such as broker IP/port and current datetime.
4. Router nodes join the FAN at a later time and update their state with periodic sync messages.

Nodes always subscribe to essential topics in order to receive such messages, while others can be optionally added.

Both BR and router nodes have a convenient touchscreen UI that can be used to compose and send messages, according to a selected topic.

### Backhaul prefix

Your uplink should provide the BR with an appropriate prefix, as noted in `border-router/mbed_app.json`.

For reference, this can be achieved with Mikrotik RouterOS using the following configuration:

```
/ipv6 dhcp-server
add address-pool=test interface=bridge name=def
/ipv6 pool
add name=test prefix=2001:db8::/64 prefix-length=64
/ipv6 address
add address=2001:db8::2 interface=bridge
/ipv6 nd
set [ find default=yes ] interface=bridge
```

Pay attention to the MAC address in the BR config which will be different in your case. Using that of the bridge should work.

### Certificate chain

A special procedure is to be followed in order to obtain certificates for each node of the network. The [following guide](https://community.silabs.com/s/article/Create-your-Own-Wi-SUN-Certificates?language=en_US) from Silicon Labs is comprehensive and can be used as a reference.

The final chain is to be included in armored format, line by line, as shown in `wisun_certificates.h` under `border-router/source` and `router/source`. Sideloading is not supported yet, so the program must be recompiled to include changes.

### Mbed applications

Both BR and router nodes have a convenient GUI, easy enough to use without too much technical knowledge. For reference, the start-up procedure is listed here.

**BR**

1. On the Home tab, press the "Start mesh network" button

![](img/start_mesh.jpg)

2. On the Settings > Network > MQTT tab, set the broker IP:port. Same for RTC datetime under Settings > Network > Time.
   1. You can change the sync interval under Settings > Network > Sync
3. On the Settings > Network > MQTT tab, connect the client to the broker

![](img/tab_network.jpg)

4. Back on the Home tab, once the state becomes "mesh ready", you can add other nodes

**Router**

1. On the Home tab, press the "Join mesh network" button

![](img/join_mesh.jpg)

2. Once the state becomes "connected", on the Settings > Sync tab, you must start the Sync process.
   1. By default, sync messages are sent every 30 seconds from the BR.

![](img/sync_menu.jpg)

3. Once the node receives sync data, on the Settings > Network > MQTT tab, connect the client to the broker

At this point, text messages may be sent and received from the Message tab on both nodes.

![](img/msg_menu.jpg)

### Flask webapp

The following steps apply as for most Flask applications:

- Create a Python venv: `python -m venv venv && source venv/bin/activate`
- Install dependencies: `pip install -U .`
- Set the FLASK_APP variable: `export FLASK_APP=radrane`
- Run the app: `flask run` which will listen on localhost:5000 by default

It can also be exposed through an appropriate Web server or reverse proxy such as nginx or haproxy, but this is outside the scope of this project.

## Development

To setup a development environment for Mbed, these steps below must be followed carefully.

### Requirements

The following systems have been tested for development purposes:

- Arch Linux
- Ubuntu 22.04

Other platforms may work too but this has not been tested.

### Install Python, SVN, Git

**Arch Linux**

```
sudo pacman -Sy --needed python-pip git svn
```

**Ubuntu 22.04**

```
sudo apt install python3-pip git svn
```

### Install Mbed CLI

Using Python's package manager, install Mbed CLI:

```
pip install -U mbed-cli
```

### Install the GNU ARM toolchain

The following toolchain version from [ARM's website](https://developer.arm.com/downloads/-/gnu-rm) has been tested and is recommended:

- 9-2020-q2-update

Other releases may work but this has not been tested.

The following Bash script fetches and unpacks the specified releases locally:

```
wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2
md5sum gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2
tar -xzf gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2
chmod -R +x gcc-arm-none-eabi-9-2020-q2-update/bin/*
```

Don't forget to check the MD5 checksum with that of the download page.

### Configure the toolchain

The `mbed` command will ask for the toolchain and target to use each time, so it is advised to configure this once, either globally or per application.

The following Bash script sets the toolchain globally (remove `-G` to set locally):

```
mbed config -G GCC_ARM_PATH $(pwd)/gcc-arm-none-eabi-9-2020-q2-update/bin/
mbed config -G ARM_PATH $(pwd)/gcc-arm-none-eabi-9-2020-q2-update/bin/
mbed config -G TOOLCHAIN GCC_ARM
```

### Importing

Provided the application is available at a remote address, you can use `mbed import URL`. It will take care of all dependencies.

### Compiling

Simply run `mbed compile` within the application directory. These flags can be added:

- `--flash` to flash after compiling, requires the target to be mounted over USB;
- `--sterm` to attach over serial to the target after flashing;
- `--baudrate N` to specify the serial baudrate.

### Flashing

If the program was not flashed with the compile command, the `.bin` file under `BUILD/<TARGET>/<TOOLCHAIN>` can be manually copied to the target, which will reset afterwards and run the new program.