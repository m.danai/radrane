#!/bin/bash

# Connect to broker on 2001:db8::3 (--mqtt-host)
# Listen on 2001:db8::3 (--host)
# Be verbose (--debug)
# Listen on port 10000 (--port)
# Set client timeout to 90s (--mqtt-timeout)
bisquitt --mqtt-host [2001:db8::3] --host [2001:db8::3] --debug --port 10000 --mqtt-timeout 90s