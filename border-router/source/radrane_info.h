#ifndef RADRANE_INFO_H
#define RADRANE_INFO_H

#ifdef __cplusplus
extern "C"
{
#endif

#define PROGRAM_NAME "RADRANE border-router"
#define VERSION_MAJOR 0
#define VERSION_MINOR 1
#define VERSION_PATCH 0

#define RELEASE_DATE "27.07.2023"

#ifdef __cplusplus
}
#endif

#endif /* RADRANE_INFO_H */