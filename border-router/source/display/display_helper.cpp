#include <stdlib.h>
#include <string.h>

#include "display_helper.h"
#include "sync_helper.h"
#include "time_helper.h"

#include "borderrouter_init.h"

#include "lvgl.h"
#include "hal_stm_lvgl/tft/tft.h"
#include "hal_stm_lvgl/touchpad/touchpad.h"
#include "stm32f769i_discovery_lcd.h"

#include "mqtt_helper.h"

#include "wisun_data.h"

#include "ip6string.h"

#include "EMACInterface.h"


/**********************
 *  STATIC PROTOTYPES
 **********************/

static EventQueue* queue;
static lv_obj_t* popup;

// custom style
static lv_style_t style;

// LVGL timing
static Ticker lvgl_ticker;
static void lvgl_ticker_cb();

// LCD management
static Timer display_timer;
static DigitalIn displayButton(BUTTON1);
static bool displayEnabled = 1;
static uint8_t last_brightness = LCD_BRIGHTNESS_DEFAULT;
static void lcd_on_off_func();
static void brightness_cb(lv_event_t* e);

// Uptime
static Timer t;
static lv_obj_t* uptime_label;
static void uptime_cb(lv_timer_t* timer);

// Time
static ds3231_time_t rtc_time;
static ds3231_calendar_t rtc_calendar;
static lv_obj_t* clock_label;
static lv_obj_t* new_datetime_label;
static lv_obj_t* sb_hours;
static lv_obj_t* sb_minutes;
static lv_obj_t* sb_seconds;
static lv_obj_t* calendar;
static void spinbox_cb(lv_event_t* e);
static void calendar_cb(lv_event_t* e);
static void update_new_datetime_label();
static void update_datetime_cb(lv_event_t * e);

// Multicast
static lv_obj_t* sync_dropdown;
static char* sync_intervals = "30 seconds\n1 minute\n5 minutes";
static const uint sync_intervals_int[] = {30, 60, 300};

// Status
static mbed_stats_sys_t stats;
static mbed_stats_heap_t heap_info;
static Ticker status_ticker;
static lv_obj_t* label_mesh_status;
static lv_obj_t* label_mem;
static lv_obj_t* label_eth0;
static lv_obj_t* label_mesh0;
static lv_obj_t* label_mqtt_client;
static nsapi_error_t last_mqtt_status = NSAPI_ERROR_NO_CONNECTION;
static void update_memory_info();
static void update_mqtt_info();
static void status_ticker_cb();
static void get_iface_addresses(char* text, int id);

// MQTT
static void update_broker_cb(lv_event_t* e);
static void broker_kb_cb(lv_event_t* e);
static void broker_connect_cb(lv_event_t* e);
static void broker_disconnect_cb(lv_event_t* e);
static char* topics = "Emergency\nGeneral";
static lv_obj_t* topic_dropdown;
static lv_obj_t* ta_ip;
static lv_obj_t* ta_port;
static lv_obj_t* btn_broker_connect;
static lv_obj_t* btn_broker_disconnect;

// Logging
static lv_obj_t* log_table;
static lv_obj_t* tabview;
static size_t log_count = 0;
static void log_tab_cb(lv_event_t* e);
static void update_log_tab_title();

// Menu help functions
static lv_obj_t * create_text(lv_obj_t * parent, const char * icon, const char * txt, lv_menu_builder_variant_t builder_variant);
static lv_obj_t * create_slider(lv_obj_t * parent, const char * icon, const char * txt, int32_t min, int32_t max, int32_t val);
static lv_obj_t* create_spinbox(lv_obj_t* parent, const char* label, int32_t min, int32_t max, lv_event_cb_t callback);

static void keyboard_event_cb(lv_event_t* e);
static void wisun_start_cb(lv_event_t* e);

/*
 * Add the given message in the log table
 */
void add_log(char *message) {
    size_t row_count = lv_table_get_row_cnt(log_table);

    char buffer[64];
    ds3231_time_t time;
    ds3231_calendar_t cal;
    get_time(&time);
    get_calendar(&cal);

    format_rtc_date_string(buffer, &time, &cal);
    lv_table_set_cell_value(log_table, row_count, 0, buffer);
    lv_table_set_cell_value(log_table, row_count, 1, message);

    log_count++;
    update_log_tab_title();
}

void start_display() {
    // start uptime timer
    t.start();

    // initialize display through LVGL
    lv_init();
    lv_port_disp_init();
    lv_port_indev_init();

    // attack LVGL ticker callback
    lvgl_ticker.attach(callback(&lvgl_ticker_cb), TICKER_TIME);
    status_ticker.attach(callback(&status_ticker_cb), TICKER_STATUS_TIME);

    // set default LCD brightness
    BSP_LCD_SetBrightness(LCD_BRIGHTNESS_DEFAULT);

    displayButton.mode(PullUp);
    display_timer.start();
    queue = mbed_event_queue();
}

void build_interface() {
    // initialize custom style
    lv_style_init(&style);
    lv_style_set_text_font(&style, &symbol_custom);

    /* Tabs */
    tabview = lv_tabview_create(lv_scr_act(), LV_DIR_TOP, 50);
    lv_obj_add_style(tabview, &style, 0);

    lv_obj_t* tab_general = lv_tabview_add_tab(tabview, LV_SYMBOL_HOME);
    lv_obj_t* tab_msg = lv_tabview_add_tab(tabview, MY_SYMBOL_COMMENTS);
    lv_obj_t* tab_log = lv_tabview_add_tab(tabview, LV_SYMBOL_BELL);
    lv_obj_add_event_cb(tab_log, log_tab_cb, LV_EVENT_ALL, tab_log);
    lv_obj_t* tab_setting = lv_tabview_add_tab(tabview, LV_SYMBOL_SETTINGS);

    /* Tab > General */

    label_mesh_status = lv_label_create(tab_general);

    lv_style_t style2;
    lv_style_init(&style2);
    lv_style_set_text_font(&style2, &lv_font_montserrat_40);
    lv_label_set_text(label_mesh_status, "State: waiting for mesh start");
    lv_label_set_long_mode(label_mesh_status, LV_LABEL_LONG_WRAP); /*Break the long lines*/
    lv_label_set_recolor(label_mesh_status, true); /*Enable re-coloring by commands in the text*/
    lv_obj_set_width(label_mesh_status, 750);
    uptime_label = lv_label_create(tab_general);
    lv_obj_align(uptime_label, LV_ALIGN_TOP_LEFT, 0, 40);
    lv_obj_add_style(uptime_label, &style2, 0);
    lv_obj_add_style(label_mesh_status, &style2, 0);

    lv_obj_t* btn_wisun_start = lv_btn_create(tab_general);
    lv_obj_add_event_cb(btn_wisun_start, wisun_start_cb, LV_EVENT_CLICKED, NULL);
    lv_obj_align(btn_wisun_start, LV_ALIGN_CENTER, 0, 0);
    lv_obj_t* label = lv_label_create(btn_wisun_start);
    lv_label_set_text(label, "Start mesh network");
    lv_obj_center(label);
    lv_obj_set_height(btn_wisun_start, 60);

    lv_timer_create(uptime_cb, 1000, NULL);

    /* Tab > Messaging */

    lv_obj_t* cont;
    lv_obj_t* kb = lv_keyboard_create(tab_msg);
    lv_obj_t* ta;
    ta = lv_textarea_create(tab_msg);
    lv_textarea_set_max_length(ta, MESSAGE_MAX_LENGTH);
    lv_obj_align(ta, LV_ALIGN_TOP_LEFT, 10, 10);
    lv_obj_add_event_cb(ta, keyboard_event_cb, LV_EVENT_ALL, kb);

    label = create_text(tab_msg, LV_SYMBOL_LIST, "Topic", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_obj_align(label, LV_ALIGN_TOP_LEFT, 10, 100);
    topic_dropdown = lv_dropdown_create(tab_msg);
    lv_dropdown_set_options(topic_dropdown, topics);
    lv_obj_align(topic_dropdown, LV_ALIGN_TOP_LEFT, 130, 100);
    lv_obj_set_width(topic_dropdown, 300);

    lv_textarea_set_placeholder_text(ta, "Enter your message (max 1000 characters)");
    lv_obj_set_size(ta, 750, 80);
    lv_keyboard_set_textarea(kb, ta);

    /* Tab > Log */
    log_table = lv_table_create(tab_log);

    /*Fill the first column*/
    lv_table_set_cell_value(log_table, 0, 0, "Time");

    /*Fill the second column*/
    lv_table_set_cell_value(log_table, 0, 1, "Message");

    /*Set a smaller height to the table. It'll make it scrollable*/
    lv_obj_set_width(log_table, MY_DISP_HOR_RES - 50);
    lv_obj_set_height(log_table, MY_DISP_VER_RES - 80);
    lv_table_set_col_width(log_table, 0, 280);
    lv_table_set_col_width(log_table, 1, 460);
    lv_obj_center(log_table);

    /* Tab > Settings */

    /* Menu */

    lv_obj_t* menu = lv_menu_create(tab_setting);

    lv_color_t bg_color = lv_obj_get_style_bg_color(menu, 0);
    if (lv_color_brightness(bg_color) > 127) {
        lv_obj_set_style_bg_color(menu, lv_color_darken(lv_obj_get_style_bg_color(menu, 0), 10), 0);
    } else {
        lv_obj_set_style_bg_color(menu, lv_color_darken(lv_obj_get_style_bg_color(menu, 0), 50), 0);
    }
    lv_menu_set_mode_root_back_btn(menu, LV_MENU_ROOT_BACK_BTN_DISABLED);
    //lv_obj_add_event_cb(menu, back_event_handler, LV_EVENT_CLICKED, menu);
    lv_obj_set_size(menu, lv_disp_get_hor_res(NULL)-40, lv_disp_get_ver_res(NULL)-60);
    lv_obj_align(menu, LV_ALIGN_BOTTOM_MID, 0, 20);

    /* Submenus */

    lv_obj_t* btn;
    lv_obj_t* section;
    lv_obj_t* section_bis;

    /* Tab > Settings > Time */

    lv_obj_t* sub_time_page = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_time_page, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);
    lv_menu_separator_create(sub_time_page);
    section = lv_menu_section_create(sub_time_page);

    // Clock label
    clock_label = create_text(section, NULL, "", LV_MENU_ITEM_BUILDER_VARIANT_1);

    /* Tab > Settings > Time > Change date & time */
    lv_obj_t* sub_time_set = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_time_set, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);

    section_bis = lv_menu_section_create(sub_time_set);
    lv_label_set_long_mode(
        lv_obj_get_child(create_text(section_bis, NULL, "Please set the time and date, then press the button at the bottom of this page.", LV_MENU_ITEM_BUILDER_VARIANT_1), 0),
        LV_LABEL_LONG_WRAP
    );

    // HMS
    sb_hours = create_spinbox(section_bis, "Hours", 0, 23, spinbox_cb);
    sb_minutes = create_spinbox(section_bis, "Minutes", 0, 59, spinbox_cb);
    sb_seconds = create_spinbox(section_bis, "Seconds", 0, 59, spinbox_cb);

    // Date
    cont = lv_menu_cont_create(section_bis);
    calendar = lv_calendar_create(cont);
    lv_obj_set_size(calendar, 450, 350);
    lv_calendar_set_today_date(calendar, 2023, 07, 23);
    lv_calendar_set_showed_date(calendar, 2023, 07);
    lv_calendar_header_dropdown_create(calendar);
    lv_obj_add_event_cb(calendar, calendar_cb, LV_EVENT_ALL, NULL);

    // Set current date time if possible
    ds3231_calendar_t cal;
    ds3231_time_t time;
    auto ret = get_time(&time);
    ret |= get_calendar(&cal);
    if (ret == 0) {
        lv_spinbox_set_value(sb_hours, time.hours);
        lv_spinbox_set_value(sb_minutes, time.minutes);
        lv_spinbox_set_value(sb_seconds, time.seconds);
        lv_calendar_set_today_date(calendar, 2000 + cal.year, cal.month, cal.date);
    }

    new_datetime_label = create_text(section_bis, NULL, "", LV_MENU_ITEM_BUILDER_VARIANT_1);
    update_new_datetime_label();

    cont = lv_menu_cont_create(section_bis);
    btn = lv_btn_create(cont);
    label = lv_label_create(btn);
    lv_label_set_text(label, "Update RTC");
    lv_obj_center(label);
    lv_obj_add_event_cb(btn, update_datetime_cb, LV_EVENT_CLICKED, NULL);

    cont = lv_menu_cont_create(section);
    btn = lv_btn_create(cont);
    label = lv_label_create(btn);
    lv_label_set_text(label, "Change");
    lv_obj_center(label);
    lv_menu_set_load_page_event(menu, btn, sub_time_set);

    /* Tab > Settings > Display */

    lv_obj_t* sub_display_page = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_display_page, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);
    lv_menu_separator_create(sub_display_page);
    section = lv_menu_section_create(sub_display_page);
    lv_obj_t* lcd_slider = create_slider(section, MY_SYMBOL_LIGHTBULB, "Brightness", LCD_BRIGHTNESS_MIN, LCD_BRIGHTNESS_MAX, LCD_BRIGHTNESS_DEFAULT);
    lv_obj_add_event_cb(lv_obj_get_child(lcd_slider, -1), brightness_cb, LV_EVENT_VALUE_CHANGED, menu);

    /* Tab > Settings > Network */

    lv_obj_t* sub_network_page = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_network_page, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);
    lv_menu_separator_create(sub_network_page);
    section = lv_menu_section_create(sub_network_page);

    /* Tab > Settings > Network > MQTT */
    lv_obj_t* sub_network_mqtt = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_network_mqtt, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);

    section_bis = lv_menu_section_create(sub_network_mqtt);

    create_text(section_bis, NULL, "Client", LV_MENU_ITEM_BUILDER_VARIANT_1);
    label_mqtt_client = lv_obj_get_child(create_text(section_bis, NULL, "Status: #ff0000 disconnected#", LV_MENU_ITEM_BUILDER_VARIANT_1), -1);
    lv_label_set_recolor(label_mqtt_client, true);

    section_bis = lv_menu_section_create(sub_network_mqtt);

    create_text(section_bis, NULL, "Broker", LV_MENU_ITEM_BUILDER_VARIANT_1);

    cont = lv_menu_cont_create(section_bis);
    label = lv_label_create(cont);
    lv_label_set_text(label, "IP");
    char ip[128];
    ta_ip = lv_textarea_create(cont);
    lv_textarea_set_placeholder_text(ta_ip, "Enter an IPv6 address");
    if (get_broker_ip(ip) != 1) {
        lv_textarea_set_text(ta_ip, ip);
    }
    lv_textarea_set_one_line(ta_ip, true);
    lv_obj_set_flex_grow(ta_ip, 1);
    lv_textarea_set_accepted_chars(ta_ip, "abcdefABCDEF0123456789:");
    lv_textarea_set_max_length(ta_ip, 39); // 8 blocks of 4 characters and 7 colons = 39 characters at most

    cont = lv_menu_cont_create(section_bis);
    label = lv_label_create(cont);
    lv_label_set_text(label, "Port");
    ta_port = lv_textarea_create(cont);
    lv_textarea_set_one_line(ta_port, true);
    char port[6];
    sprintf(port, "%u", get_broker_port());
    lv_textarea_set_text(ta_port, port);
    lv_obj_set_flex_grow(ta_port, 1);
    lv_textarea_set_accepted_chars(ta_port, "0123456789");
    lv_textarea_set_max_length(ta_port, 5);
    lv_obj_add_event_cb(ta_port, broker_kb_cb, LV_EVENT_ALL, kb);

    cont = lv_menu_cont_create(section_bis);
    kb = lv_keyboard_create(cont);
    lv_obj_set_height(kb, 160);
    lv_keyboard_set_textarea(kb, NULL);
    lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN); // hide keyboard initially
    lv_obj_add_event_cb(ta_ip, broker_kb_cb, LV_EVENT_ALL, kb);
    lv_obj_add_event_cb(ta_port, broker_kb_cb, LV_EVENT_ALL, kb);

    cont = lv_menu_cont_create(section_bis);
    btn = lv_btn_create(cont);
    label = lv_label_create(btn);
    lv_label_set_text(label, "Update");
    lv_obj_center(label);
    lv_obj_add_event_cb(btn, update_broker_cb, LV_EVENT_CLICKED, NULL);

    btn_broker_connect = lv_btn_create(cont);
    lv_obj_add_event_cb(btn_broker_connect, broker_connect_cb, LV_EVENT_CLICKED, NULL);
    label = lv_label_create(btn_broker_connect);
    lv_label_set_text(label, "Connect");
    lv_obj_center(label);

    btn_broker_disconnect = lv_btn_create(cont);
    lv_obj_add_event_cb(btn_broker_disconnect, broker_disconnect_cb, LV_EVENT_CLICKED, NULL);
    label = lv_label_create(btn_broker_disconnect);
    lv_label_set_text(label, "Disconnect");
    lv_obj_add_flag(btn_broker_disconnect, LV_OBJ_FLAG_HIDDEN); // hide disconnect initially
    lv_obj_center(label);

    cont = create_text(section, MY_SYMBOL_SERVER, "MQTT", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_network_mqtt);

    /* Tab > Settings > Network > Mesh */
    lv_obj_t* sub_network_mesh = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_network_mesh, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);

    section_bis = lv_menu_section_create(sub_network_mesh);
    create_text(section_bis, NULL, "Backhaul", LV_MENU_ITEM_BUILDER_VARIANT_1);
    label_eth0 = lv_obj_get_child(create_text(section_bis, NULL, "Status: #ff0000 down#", LV_MENU_ITEM_BUILDER_VARIANT_1), -1);
    lv_label_set_recolor(label_eth0, true);

    section_bis = lv_menu_section_create(sub_network_mesh);
    create_text(section_bis, NULL, "Wi-SUN", LV_MENU_ITEM_BUILDER_VARIANT_1);
    label_mesh0 = lv_obj_get_child(create_text(section_bis, NULL, "Status: #ff0000 down#", LV_MENU_ITEM_BUILDER_VARIANT_1), -1);
    lv_label_set_recolor(label_mesh0, true);
    lv_obj_align(section_bis, LV_ALIGN_CENTER, 0, 40);

    cont = create_text(section, MY_SYMBOL_SITEMAP, "Mesh", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_network_mesh);

    /* Tab > Settings > Network > Sync */
    lv_obj_t* sub_network_sync = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_network_sync, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);

    section_bis = lv_menu_section_create(sub_network_sync);

    cont = lv_menu_cont_create(section_bis);

    lv_obj_t* sync_label = lv_label_create(cont);
    lv_label_set_text(sync_label, "Sync interval");

    sync_dropdown = lv_dropdown_create(cont);
    lv_dropdown_set_options(sync_dropdown, sync_intervals);
    lv_obj_set_flex_grow(sync_dropdown, 1);

    btn = lv_btn_create(cont);
    lv_obj_add_event_cb(btn, sync_cb, LV_EVENT_CLICKED, NULL);
    label = lv_label_create(btn);
    lv_label_set_text(label, "Set");

    lv_obj_align(section_bis, LV_ALIGN_CENTER, 0, 40);

    cont = create_text(section, MY_SYMBOL_SATELLITE_DISH, "Sync", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_network_sync);

    /* Tab > Settings > System */
    lv_obj_t* sub_system_page = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_system_page, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);
    lv_menu_separator_create(sub_system_page);
    section = lv_menu_section_create(sub_system_page);
    lv_obj_set_height(section, 160);
    label_mem = create_text(section, NULL, "%d", LV_MENU_ITEM_BUILDER_VARIANT_1);

    /* Tab > Settings > About */

    lv_obj_t* sub_software_info_page = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_software_info_page, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);
    section = lv_menu_section_create(sub_software_info_page);
    char version[29];
    snprintf(version, 29, "%s v%d.%d.%d", PROGRAM_NAME, VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);
    char release[25];
    sprintf(release, "Release date: %s", RELEASE_DATE);
    create_text(section, NULL, version, LV_MENU_ITEM_BUILDER_VARIANT_1);
    create_text(section, NULL, release, LV_MENU_ITEM_BUILDER_VARIANT_1);

    lv_obj_t* sub_about_page = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_about_page, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);
    lv_menu_separator_create(sub_about_page);
    section = lv_menu_section_create(sub_about_page);
    cont = create_text(section, NULL, "Software information", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_software_info_page);

    /*Create a root page*/
    lv_obj_t* root_page = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(root_page, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);
    section = lv_menu_section_create(root_page);

    /* Create menus */
    cont = create_text(section, MY_SYMBOL_CLOCK, "Time", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_time_page);
    cont = create_text(section, MY_SYMBOL_DESKTOP, "Display", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_display_page);
    cont = create_text(section, MY_SYMBOL_ETHERNET, "Network", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_network_page);
    cont = create_text(section, MY_SYMBOL_SERVER, "System", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_system_page);


    create_text(root_page, NULL, "Others", LV_MENU_ITEM_BUILDER_VARIANT_1);
    section = lv_menu_section_create(root_page);
    cont = create_text(section, NULL, "About", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_about_page);

    lv_menu_set_sidebar_page(menu, root_page);

    lv_event_send(lv_obj_get_child(lv_obj_get_child(lv_menu_get_cur_sidebar_page(menu), 0), 0), LV_EVENT_CLICKED,
                      NULL);
}

bool display_enabled() {
    return displayEnabled;
}

/**********************
 *   STATIC FUNCTIONS
 **********************/

/*
 * Callback function for lvgl timing.
 * Call lv_tick_inc(x) every x milliseconds in a Timer or Task (x should be between 1 and 10).
 * It is required for the internal timing of LittlevGL.
 */
static void lvgl_ticker_cb(){
    if (displayEnabled) {
        lv_tick_inc(LVGL_TICK);
    }

    if (displayButton.read()) {
        queue->call(lcd_on_off_func);
    }
}

/* LCD & display functions */

/*
 * Handles the LCD on/off button events.
 */
static void lcd_on_off_func() {
    auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(display_timer.elapsed_time()).count();

    if (diff > 1000) {
        display_timer.stop();
        display_timer.reset();

        if (displayEnabled) {
            BSP_LCD_SetBrightness(0);
            BSP_LCD_DisplayOff();
        } else {
            BSP_LCD_DisplayOn();
            BSP_LCD_SetBrightness(last_brightness);
        }

        displayEnabled = !displayEnabled;
        display_timer.start();
    }
}

/*
 * Sets the LCD brightness according to the slider value
 */
static void brightness_cb(lv_event_t * e) {
    last_brightness = lv_slider_get_value(lv_event_get_target(e));
    BSP_LCD_SetBrightness(last_brightness);
}

/*
 * Refreshes various status labels
 */
static void status_ticker_cb() {
    auto eth0 =  get_ws_eth0();
    auto mesh0 =  get_ws_mesh0();
    uint16_t addr_count;

    if (eth0 != NULL) {
        if (arm_net_interface_address_list_size(eth0->id, &addr_count) == 0) {
            char text[34 + strlen(eth0->interface_name) + addr_count * (3 + 128) + 1];
            sprintf(text, "Status: #009e00 up# (%s)\n", eth0->interface_name);

            if (addr_count > 0) {
                get_iface_addresses(text, eth0->id);
            }

            lv_label_set_text(label_eth0, text);
        }
    }

    if (mesh0 != NULL) {
        if (arm_net_interface_address_list_size(mesh0->id, &addr_count) == 0) {
            char text[34 + strlen(mesh0->interface_name) + addr_count * (3 + 128) + 1];
            sprintf(text, "Status: #009e00 up# (%s)\n", mesh0->interface_name);

            if (addr_count > 0) {
                get_iface_addresses(text, mesh0->id);
            }

            lv_label_set_text(label_mesh0, text);
        }
    }

    queue->call(update_mqtt_info);

    queue->call(update_memory_info);
    /*int status = iface->get_connection_status();
    switch (status) {
        case NSAPI_STATUS_LOCAL_UP:
            color = lv_palette_main(LV_PALETTE_GREEN);
            break;
        case NSAPI_STATUS_GLOBAL_UP:
            color = lv_palette_main(LV_PALETTE_GREEN);
            break;
        case NSAPI_STATUS_DISCONNECTED:
            color = lv_palette_main(LV_PALETTE_RED);
            break;
        case NSAPI_STATUS_CONNECTING:
            color = lv_palette_main(LV_PALETTE_YELLOW);
            break;
    }*/
}

static void update_memory_info() {
    mbed_stats_sys_get(&stats);
    mbed_stats_heap_get(&heap_info);
    lv_label_set_text_fmt(lv_obj_get_child(label_mem, 0), "Bytes allocated currently: %ld", heap_info.current_size);
}

static void update_mqtt_info() {
    auto mqtt_status = is_client_connected();
    switch (mqtt_status) {
        case NSAPI_ERROR_OK:
            lv_label_set_text(label_mqtt_client, "Status: #009e00 connected#");
            if (mqtt_status != last_mqtt_status) {
                add_log("MQTT: connected to broker");
            }
            break;
        case NSAPI_ERROR_CONNECTION_LOST:
            lv_label_set_text(label_mqtt_client, "Status: #ff0000 connection lost#");
            if (mqtt_status != last_mqtt_status) {
                add_log("MQTT: lost connection to broker");
            }
            break;
        case NSAPI_ERROR_NO_CONNECTION:
            lv_label_set_text(label_mqtt_client, "Status: #ff0000 disconnected#");
            if (mqtt_status != last_mqtt_status) {
                add_log("MQTT: disconnected from broker");
            }
            break;
    }
    last_mqtt_status = mqtt_status;
}

/*
 * Clear the log count when tab is opened
 */
static void log_tab_cb(lv_event_t* e) {
    if (lv_tabview_get_tab_act(tabview) != 2) return;

    log_count = 0;
    update_log_tab_title();
}

/*
 * Update the log tab title with the current log count (if nonzero)
 */
static void update_log_tab_title() {
    char buffer[32];
    sprintf(buffer, "%s", LV_SYMBOL_BELL);
    if (log_count > 0) {
        sprintf(buffer + 3, " %d", log_count);
    }
    lv_tabview_rename_tab(tabview, 2, buffer);
}

/*
 * Start the broker connection
 */
static void broker_connect_cb(lv_event_t* e) {
    NetworkInterface* iface;
    iface = EthInterface::get_default_instance();

    auto ret = open_socket(iface);
    if (ret != NSAPI_ERROR_OK) {
        popup = lv_msgbox_create(NULL, "Error", "Backhaul interface is down.", {NULL}, true);
        lv_obj_set_width(popup, 600);
        lv_obj_center(popup);
        return;
    }

    char ip[128];
    get_broker_ip(ip);
    ret = connect_socket(ip, get_broker_port());
    if (ret != NSAPI_ERROR_OK) {
        popup = lv_msgbox_create(NULL, "Error", "Broker unreachable. Check IP settings and try again later.", {NULL}, true);
        lv_obj_center(popup);
        return;
    }

    ret = connect_client();
    if (ret != NSAPI_ERROR_OK) {
        popup = lv_msgbox_create(NULL, "Error", "Connection failed, check broker logs.", {NULL}, true);
        lv_obj_center(popup);
        return;
    }

    popup = lv_msgbox_create(NULL, "Success", "Connected to broker", {NULL}, true);
    lv_obj_center(popup);

    lv_obj_add_flag(btn_broker_connect, LV_OBJ_FLAG_HIDDEN);
    lv_obj_clear_flag(btn_broker_disconnect, LV_OBJ_FLAG_HIDDEN);
}

/*
 * End the broker connection
 */
static void broker_disconnect_cb(lv_event_t* e) {
    disconnect_client();

    // swap buttons
    lv_obj_add_flag(btn_broker_disconnect, LV_OBJ_FLAG_HIDDEN);
    lv_obj_clear_flag(btn_broker_connect, LV_OBJ_FLAG_HIDDEN);

    popup = lv_msgbox_create(NULL, "Success", "Disconnected from broker", {NULL}, true);
    lv_obj_center(popup);
}

/*
 * Start the Wi-SUN stack
 */
static void wisun_start_cb(lv_event_t* e) {
    if(lv_event_get_code(e) == LV_EVENT_CLICKED) {
        char ip[128];
        int ret = get_broker_ip(ip);
        if (ret == 1) {
            popup = lv_msgbox_create(NULL, "Error", "Please set the MQTT broker IP first.", {NULL}, true);
            lv_obj_center(popup);
        } else {
            start_border_router();
            add_log("Mesh started");
            lv_obj_del(lv_event_get_target(e));
            lv_label_set_text(label_mesh_status, "State: #0000ff booting#");
        }
    }
}

/*
 * Updates the uptime and clock labels
 */
static void uptime_cb(lv_timer_t* timer) {
    auto us = t.elapsed_time();
    lv_label_set_text_fmt(
        uptime_label,
        "Uptime: %0*llu:%0*llu:%0*llu",
        2, std::chrono::duration_cast<std::chrono::hours>(us).count(),
        2, std::chrono::duration_cast<std::chrono::minutes>(us).count() % 60,
        2, std::chrono::duration_cast<std::chrono::seconds>(us).count() % 60
    );

    auto ret = get_time(&rtc_time);
    ret |= get_calendar(&rtc_calendar);
    if (ret == 0) {
        lv_label_set_text_fmt(
            lv_obj_get_child(clock_label, 0),
            "Current RTC clock: %0*lu.%0*lu.20%0*lu %0*lu:%0*lu:%0*lu",
            2, rtc_calendar.date,
            2, rtc_calendar.month,
            2, rtc_calendar.year,
            2, rtc_time.hours,
            2, rtc_time.minutes,
            2, rtc_time.seconds
        );
    }
}

/*
 * Handle text input for MQTT messaging
 */
static void keyboard_event_cb(lv_event_t * e) {
    lv_obj_t * ta = lv_event_get_target(e);

    if (lv_event_get_code(e) == LV_EVENT_READY) {
        auto client_status = is_client_connected();

        if (client_status == NSAPI_ERROR_NO_CONNECTION or client_status == NSAPI_ERROR_CONNECTION_LOST) {
            popup = lv_msgbox_create(NULL, "Error", "Disconnected from MQTT broker.", {NULL}, true);
            lv_obj_center(popup);
        } else {
            char* message = (char*) lv_textarea_get_text(ta);

            if (strlen(message) == 0) {
                popup = lv_msgbox_create(NULL, "Error", "Your message is empty.", {NULL}, true);
                lv_obj_center(popup);
            } else {
                char topic[64];
                lv_dropdown_get_selected_str(topic_dropdown, topic, sizeof(topic) / sizeof(char));
                publish(topic, message);
            }
        }
    }
}

/*
 * Register a new calendar date selection for RTC updation
 */
static void calendar_cb(lv_event_t* e) {
    lv_obj_t* cal = lv_event_get_current_target(e);

    if (lv_event_get_code(e) == LV_EVENT_VALUE_CHANGED) {
        lv_calendar_date_t date;
        if(lv_calendar_get_pressed_date(cal, &date)) {
            lv_calendar_set_today_date(cal, date.year, date.month, date.day);
            update_new_datetime_label();
        }
    }
}

/*
 * Step the value of the target spinbox
 */
static void spinbox_cb(lv_event_t* e) {
    lv_obj_t* target = (lv_obj_t*) lv_event_get_user_data(e);

    if (lv_obj_get_style_bg_img_src(lv_event_get_current_target(e), 0) == LV_SYMBOL_PLUS) {
        lv_spinbox_increment(target);
    } else {
        lv_spinbox_decrement(target);
    }

    update_new_datetime_label();
}

/*
 * Updates the new date time label with form data
 */
static void update_new_datetime_label() {
    auto date = lv_calendar_get_today_date(calendar);
    lv_label_set_text_fmt(
        lv_obj_get_child(new_datetime_label, 0),
        "New date & time: %02d.%02d.%d %02ld:%02ld:%02ld",
        date->day,
        date->month,
        date->year,
        lv_spinbox_get_value(sb_hours),
        lv_spinbox_get_value(sb_minutes),
        lv_spinbox_get_value(sb_seconds)
    );
}

/*
 * Updates the RTC with the form data
 */
static void update_datetime_cb(lv_event_t * e) {
    ds3231_time_t time = {0, 0, 0, 0, 0};
    ds3231_calendar_t cal = {0, 0, 0, 0};
    auto date = lv_calendar_get_today_date(calendar);

    time.hours = lv_spinbox_get_value(sb_hours);
    time.minutes = lv_spinbox_get_value(sb_minutes);
    time.seconds = lv_spinbox_get_value(sb_seconds);
    cal.date = date->day;
    cal.month = date->month;
    cal.year = date->year - 2000;
    cal.day = get_week_day(cal.date, cal.month, date->year);

    auto ret = set_datetime(cal, time);
    if (ret == 0) {
        popup = lv_msgbox_create(NULL, "Success", "RTC has been updated.", {NULL}, true);
        char msg[128];
        char before[48];
        char after[48];
        format_rtc_date_string(before, &rtc_time, &rtc_calendar);
        format_rtc_date_string(after, &time, &cal);
        sprintf(
            msg,
            "RTC updated %s -> %s",
            before, after
        );
        add_log(msg);
    } else {
        popup = lv_msgbox_create(NULL, "Error", "Could not update RTC, try again later.", {NULL}, true);
    }

    lv_obj_center(popup);
}

/*
 * Proxy callback to trigger the sync mechanism
 */
void sync_cb(lv_event_t* e) {
    if (e == NULL) {
        auto ret = init_sync();
        if (ret != NSAPI_ERROR_OK) {
            popup = lv_msgbox_create(NULL, "Error", "Could not start sync process.", {NULL}, true);
            lv_obj_center(popup);
        } else {
            std::chrono::microseconds interval(30s);
            set_sync_interval(interval);
            lv_label_set_text(label_mesh_status, "State: #009e00 mesh ready#");
        }
    } else {
        uint interval = sync_intervals_int[lv_dropdown_get_selected(sync_dropdown)];
        auto interval_us = std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::seconds(interval)
        );

        set_sync_interval(interval_us);

        char buffer[48];
        sprintf(buffer, "Set sync interval to %d seconds", interval);

        add_log(buffer);
    }
}

/*
 * Update broker IP:port when button is clicked
 */
static void update_broker_cb(lv_event_t* e) {
    const char* port = lv_textarea_get_text(ta_port);
    const char* ip = lv_textarea_get_text(ta_ip);

    u_int16_t port_num = (u_int16_t) atoi(port);
    set_broker_port(port_num);

    // no further parsing or pre-processing
    set_broker_ip((char*) ip);

    char log[60];
    sprintf(log, "Set broker to %s:%s", ip, port);
    add_log(log);
}

/*
 * Handle text area input for broker IP:port
 */
static void broker_kb_cb(lv_event_t* e) {
    lv_obj_t* ta = lv_event_get_target(e);
    lv_obj_t* kb = (lv_obj_t*) lv_event_get_user_data(e);
    auto code = lv_event_get_code(e);

    if (code == LV_EVENT_FOCUSED) {
            lv_keyboard_set_textarea(kb, ta);
            lv_obj_clear_flag(kb, LV_OBJ_FLAG_HIDDEN);
    } else if (code == LV_EVENT_DEFOCUSED) {
            lv_keyboard_set_textarea(kb, NULL);
            lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);
    } else if (code == LV_EVENT_READY) {
            char* message = (char*) lv_textarea_get_text(ta);

            if (ta == ta_port) {
                int port = atoi(message);
                if (port < 1 || port > 65535) {
                    popup = lv_msgbox_create(NULL, "Error", "Port must be within 1-65535.", {NULL}, true);
                    lv_obj_center(popup);
                    lv_textarea_set_text(ta, "");
                }
            } else {
                // IPv6 validation here
            }
    }
}

/* Helper functions */

static void get_iface_addresses(char* text, int id) {
    uint8_t address_buf[128];
    int address_count = 0;

    if (arm_net_address_list_get(id, 128, address_buf, &address_count) == 0) {
        size_t written = strlen(text);
        char buf[128];

        sprintf(text + written, "Addresses:");
        written += 10;

        uint8_t *t_buf = address_buf;
        for (int i = 0; i < address_count; ++i) {
            ip6tos(t_buf, buf);
            t_buf += 16;
            sprintf(text + written, "\n- %s", buf);
            written += 3 + strlen(buf);
        }
    }
}

static lv_obj_t * create_text(lv_obj_t * parent, const char * icon, const char * txt, lv_menu_builder_variant_t builder_variant) {
    lv_obj_t * obj = lv_menu_cont_create(parent);

    lv_obj_t * img = NULL;
    lv_obj_t * label = NULL;

    if(icon) {
        img = lv_img_create(obj);
        lv_img_set_src(img, icon);
    }

    if(txt) {
        label = lv_label_create(obj);
        lv_label_set_text(label, txt);
        lv_label_set_long_mode(label, LV_LABEL_LONG_SCROLL_CIRCULAR);
        lv_obj_set_flex_grow(label, 1);
    }

    if(builder_variant == LV_MENU_ITEM_BUILDER_VARIANT_2 && icon && txt) {
        lv_obj_add_flag(img, LV_OBJ_FLAG_FLEX_IN_NEW_TRACK);
        lv_obj_swap(img, label);
    }

    return obj;
}

static lv_obj_t * create_slider(lv_obj_t * parent, const char * icon, const char * txt, int32_t min, int32_t max, int32_t val) {
    lv_obj_t * obj = create_text(parent, icon, txt, LV_MENU_ITEM_BUILDER_VARIANT_2);

    lv_obj_t * slider = lv_slider_create(obj);
    lv_obj_set_flex_grow(slider, 1);
    lv_slider_set_range(slider, min, max);
    lv_slider_set_value(slider, val, LV_ANIM_OFF);

    if(icon == NULL) {
        lv_obj_add_flag(slider, LV_OBJ_FLAG_FLEX_IN_NEW_TRACK);
    }

    return obj;
}

static lv_obj_t* create_spinbox(lv_obj_t* parent, const char* label, int32_t min, int32_t max, lv_event_cb_t callback) {
    lv_obj_t * cont = lv_menu_cont_create(parent);

    lv_obj_t* sb_label = lv_label_create(cont);
    lv_label_set_text(sb_label, label);

    lv_obj_t* sb = lv_spinbox_create(cont);
    lv_spinbox_set_rollover(sb, true);
    lv_spinbox_set_cursor_pos(sb, 0);
    lv_spinbox_set_range(sb, min, max);
    lv_spinbox_set_digit_format(sb, 2, 0);
    lv_obj_set_width(sb, 100);
    lv_obj_center(sb);

    lv_coord_t h = lv_obj_get_height(sb);

    lv_obj_t* btn = lv_btn_create(cont);
    lv_obj_set_size(btn, h, h);
    lv_obj_align_to(btn, sb, LV_ALIGN_OUT_LEFT_MID, -5, 0);
    lv_obj_set_style_bg_img_src(btn, LV_SYMBOL_MINUS, 0);
    lv_obj_add_event_cb(btn, callback, LV_EVENT_CLICKED, sb);

    btn = lv_btn_create(cont);
    lv_obj_set_size(btn, h, h);
    lv_obj_align_to(btn, sb, LV_ALIGN_OUT_RIGHT_MID, 5, 0);
    lv_obj_set_style_bg_img_src(btn, LV_SYMBOL_PLUS, 0);
    lv_obj_add_event_cb(btn, callback, LV_EVENT_CLICKED, sb);

    return sb;
}
