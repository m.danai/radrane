#ifndef DISPLAY_HELPER_H
#define DISPLAY_HELPER_H

#include "nsconfig.h"
#include "protocol.h"

#include "radrane_info.h"

#include "lvgl.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define LVGL_TICK   10                               //Time tick value for lvgl in ms (1-10msa)
#define TICKER_TIME 10ms                             //modified to miliseconds
#define TICKER_STATUS_TIME 1000ms
#define LCD_BRIGHTNESS_DEFAULT 50 // set LCD to half brightness
#define LCD_BRIGHTNESS_MIN 10
#define LCD_BRIGHTNESS_MAX 100
#define MESSAGE_MAX_LENGTH 1000

enum {
    LV_MENU_ITEM_BUILDER_VARIANT_1,
    LV_MENU_ITEM_BUILDER_VARIANT_2
};
typedef uint8_t lv_menu_builder_variant_t;

void start_display();
void build_interface();
bool display_enabled();
void add_log(char* message);
void sync_cb(lv_event_t* e);

#ifdef __cplusplus
}
#endif

#endif /* DISPLAY_HELPER_H */
