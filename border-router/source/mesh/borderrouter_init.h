/*
 * Copyright (c) 2016-2018, Pelion and affiliates.
 */


#ifndef BORDERROUTER_INIT_H
#define BORDERROUTER_INIT_H

#ifdef __cplusplus
extern "C"
{
#endif

void start_border_router();

#ifdef __cplusplus
}
#endif

#endif /* BORDERROUTER_INIT_H */
