#ifndef WISUN_DATA_H
#define WISUN_DATA_H

#include "nsconfig.h"
#include "protocol.h"

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct {
    int DODAG_ID[16];
    int rpl_instance_id;
    int rpl_setups;
    char* network_name;
    protocol_interface_info_entry_t* mesh0;
    protocol_interface_info_entry_t* eth0;
} wisun_data_t;

void set_ws_mesh0(protocol_interface_info_entry_t* ptr);
protocol_interface_info_entry_t* get_ws_mesh0();
void set_ws_eth0(protocol_interface_info_entry_t* ptr);
protocol_interface_info_entry_t* get_ws_eth0();

#ifdef __cplusplus
}
#endif

#endif /* WISUN_DATA_H */