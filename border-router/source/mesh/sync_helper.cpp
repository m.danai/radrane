#include <stdlib.h>
#include "sync_helper.h"
#include "time_helper.h"
#include "display_helper.h"
#include "mqtt_helper.h"

#include "mbed.h"

static EventQueue* queue;

static Ticker sync_ticker;
static bool ticker_set = false;

static UDPSocket socket;
static bool socket_ready = false;
static const SocketAddress multicast_group(SYNC_MCAST_GROUP, SYNC_PORT);

static void sync_message_proxy();
static void send_sync_message();

nsapi_error_t init_sync() {
    if (socket_ready){
        return NSAPI_ERROR_OK;
    }

    auto ret = socket.open(NetworkInterface::get_default_instance());
    if (ret != NSAPI_ERROR_OK) {
        return ret;
    }

    ret = socket.join_multicast_group(multicast_group);

    socket.set_blocking(false);

    if (ret == NSAPI_ERROR_OK) {
        socket_ready = true;
        queue = mbed_event_queue();
        add_log("Sync process started");
    }

    return ret;
}

void set_sync_interval(std::chrono::microseconds interval) {
    if (ticker_set) {
        sync_ticker.detach();
    } else {
        ticker_set = true;
    }

    sync_ticker.attach(callback(&sync_message_proxy), interval);
}

static void sync_message_proxy() {
    queue->call(send_sync_message);
}

static void send_sync_message() {
    ds3231_calendar_t cal;
    ds3231_time_t time;
    get_time(&time);
    get_calendar(&cal);

    sync_data_t data;
    char ip[128];
    get_broker_ip(ip);
    SocketAddress broker(ip, get_broker_port());
    data.calendar = cal;
    data.time = time;
    data.broker = broker;

    socket.sendto(multicast_group, &data, sizeof(data));
}
