#include <stdlib.h>
#include <stdio.h>
#include "wisun_data.h"

static wisun_data_t wisun_data;

void set_ws_mesh0(protocol_interface_info_entry_t* ptr) {
    wisun_data.mesh0 = ptr;
}

protocol_interface_info_entry_t* get_ws_mesh0() {
    return wisun_data.mesh0;
}

void set_ws_eth0(protocol_interface_info_entry_t* ptr) {
    wisun_data.eth0 = ptr;
}

protocol_interface_info_entry_t* get_ws_eth0() {
    return wisun_data.eth0;
}
