#include "time_helper.h"

static Ds3231* rtc;

void init_rtc(PinName sda, PinName scl) {
    rtc = new Ds3231(sda, scl);
}

uint16_t get_calendar(ds3231_calendar_t* cal) {
    return rtc->get_calendar(cal);
}

uint16_t get_time(ds3231_time_t* time) {
    return rtc->get_time(time);
}

uint16_t set_datetime(ds3231_calendar_t cal, ds3231_time_t time) {
    uint16_t ret = rtc->set_time(time);
    return ret | rtc->set_calendar(cal);
}

/*
 * Format a DD.MM.YYYY HH:MM:SS date string into buffer, which has to be 48 bytes long
 */
void format_rtc_date_string(char* buffer, ds3231_time_t* time, ds3231_calendar_t* cal) {
    sprintf(
        buffer,
        "%02lu.%02lu.20%02lu %02lu:%02lu:%02lu",
        cal->date,
        cal->month,
        cal->year,
        time->hours,
        time->minutes,
        time->seconds
    );
}

/*
 * Returns the weekday for the given date, 1 being Sunday
 *
 * Based on the guide from Claus Tøndering: https://www.tondering.dk/claus/cal/chrweek.php#calcdow
 */
size_t get_week_day(uint day, uint month, uint year) {
    static int t[] = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 };
    auto y = year - month < 3;
    return (1 + (( y + y / 4 - y / 100 + y / 400 + t[month - 1] + day) % 7)) % 8;
}
