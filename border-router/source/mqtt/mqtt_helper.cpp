#include "mqtt_helper.h"

#include "mbed.h"
#include <sys/types.h>

static TCPSocket socket;
//static UDPSocket socket;
static SocketAddress sockAddr;
static MQTTClient* client;
static MQTTPacket_connectData mqtt_settings = MQTTPacket_connectData_initializer;
//static MQTTSNPacket_connectData mqttsn_settings = MQTTSNPacket_connectData_initializer;

static Ticker keepalive_ticker;
static EventQueue* queue;
static Mutex reset_mutex;

u_int16_t broker_port = 1883;
char broker_ip[128] = {0};

static void dirty_keepalive();

nsapi_error_t open_socket(NetworkInterface* iface) {
    return socket.open(iface);
}

nsapi_error_t connect_socket(char* ip, int port) {
    sockAddr.set_ip_address(ip);
    sockAddr.set_port(port);
    return socket.connect(sockAddr);
}

int connect_client() {
    if (client == NULL) {
        client = new MQTTClient(&socket);
    }

    if (client->connect(mqtt_settings) != NSAPI_ERROR_OK) {
        return 1;
    }

    if (queue == NULL) {
        queue = mbed_event_queue();
    }

    keepalive_ticker.attach(callback(&dirty_keepalive), KEEPALIVE_TIME);
    client->subscribe(TOPIC_EMERGENCY, MQTT::QOS1, message_cb);
    client->setDefaultMessageHandler(message_cb);

    return 0;
}

void disconnect_client() {
    keepalive_ticker.detach();

    // free client resources
    client->disconnect();

    socket.close();
}

void publish(char* topic, char* message, MQTT::QoS qos, bool dup) {
    MQTT::Message msg;
    msg.qos = qos;
    msg.payload = message;
    msg.payloadlen = strlen(message);
    msg.retained = 0;
    msg.dup = dup;

    client->publish(topic, msg);
}

void reset_client() {
    // don't reset if another reset is ongoing
    if (!reset_mutex.trylock()) {
        return;
    };

    disconnect_client();

    // start socket anew
    open_socket(EthInterface::get_default_instance());
    connect_socket(broker_ip, broker_port);

    // create new client
    client = new MQTTClient(&socket);
    connect_client();

    reset_mutex.unlock();
}

static void dirty_keepalive() {
    if (is_client_connected() != NSAPI_ERROR_OK) {
        queue->call(reset_client);
    } else {
        char* payload = "A";
        queue->call(publish, payload, payload, MQTT::QOS2, true);
    }
}

nsapi_error_t is_client_connected() {
    if (client != NULL) {
        return client->isConnected() ? NSAPI_ERROR_OK : NSAPI_ERROR_CONNECTION_LOST;
    }
    return NSAPI_ERROR_NO_CONNECTION;
}

void message_cb(MQTT::MessageData& msgData) {
    printf("[%s][Q%d]: %s\n", msgData.topicName.cstring, msgData.message.qos, (char*) msgData.message.payload);
}

u_int16_t get_broker_port() {
    return broker_port;
}

void set_broker_port(u_int16_t port) {
    broker_port = port;
}

int get_broker_ip(char* dest) {
    if (broker_ip[0] == 0) {
        return 1;
    }
    strncpy(dest, broker_ip, sizeof(broker_ip) / sizeof(char));
    return 0;
}

void set_broker_ip(char* ip) {
    strncpy(broker_ip, ip, sizeof(broker_ip) / sizeof(char));
}
