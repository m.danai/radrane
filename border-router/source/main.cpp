#include "mbed.h"

#include "time_helper.h"
#include "lvgl.h"
#include "display/display_helper.h"


int main() {
    init_rtc(D14, D15);

    start_display();
    build_interface();

    while (true){
        if (display_enabled()) {
            lv_task_handler();
        }
        //Call lv_task_handler() periodically every few milliseconds.
        //It will redraw the screen if required, handle input devices etc.
        thread_sleep_for(LVGL_TICK);
    }
}
