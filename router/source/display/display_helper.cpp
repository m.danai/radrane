#include <stdlib.h>
#include <string.h>

#include "display_helper.h"
#include "sync_helper.h"
#include "mesh_helper.h"

#include "lvgl.h"
#include "hal_stm_lvgl/tft/tft.h"
#include "hal_stm_lvgl/touchpad/touchpad.h"
#include "stm32f769i_discovery_lcd.h"

#include "mqtt_helper.h"

#include "ip6string.h"

/**********************
 *  STATIC PROTOTYPES
 **********************/

static Thread thread;
static EventQueue* queue;
static lv_obj_t* popup;

// custom style
static lv_style_t style;

// LVGL timing
static Ticker lvgl_ticker;
static void lvgl_ticker_cb();

// LCD management
static Timer display_timer;
static DigitalIn displayButton(BUTTON1);
static bool displayEnabled = 1;
static uint8_t last_brightness = LCD_BRIGHTNESS_DEFAULT;
static void lcd_on_off_func();
static void brightness_cb(lv_event_t* e);

// Uptime
static Timer t;
static lv_obj_t* uptime_label;
static void uptime_cb(lv_timer_t* timer);

// Time
static lv_obj_t* clock_label;
static lv_obj_t* new_datetime_label;
static void update_new_datetime_label();
static void update_datetime_cb(lv_event_t * e);

// Status
static mbed_stats_sys_t stats;
static mbed_stats_heap_t heap_info;
static Ticker status_ticker;
static lv_obj_t* label_mesh_status;
static lv_obj_t* label_mem;
static lv_obj_t* label_mesh0;
static lv_obj_t* label_mqtt_client;
static nsapi_error_t last_mqtt_status = NSAPI_ERROR_NO_CONNECTION;
static void update_memory_info();
static void update_mqtt_info();
static void status_ticker_cb();
static void update_mesh_info();

// MQTT
static void update_broker_cb(lv_event_t* e);
static void broker_connect_cb(lv_event_t* e);
static void broker_disconnect_cb(lv_event_t* e);
static void connect_to_broker();
static char* topics = "Emergency\nGeneral";
static lv_obj_t* topic_dropdown;
static lv_obj_t* btn_broker_connect;
static lv_obj_t* btn_broker_disconnect;
static lv_obj_t* label_broker_ip;
static lv_obj_t* label_broker_port;

// Mesh
static lv_obj_t* btn_mesh_connect;
static lv_obj_t* btn_mesh_disconnect;
static lv_obj_t* btn_sync;
static lv_obj_t* sync_label;
static void wisun_start_cb(lv_event_t* e);
static void wisun_disconnect_cb(lv_event_t* e);
static void sync_cb(lv_event_t* e);

// Logging
static lv_obj_t* log_table;
static lv_obj_t* tabview;
static size_t log_count = 0;
static void log_tab_cb(lv_event_t* e);
static void update_log_tab_title();

// Menu help functions
static lv_obj_t * create_text(lv_obj_t * parent, const char * icon, const char * txt, lv_menu_builder_variant_t builder_variant);
static lv_obj_t * create_slider(lv_obj_t * parent, const char * icon, const char * txt, int32_t min, int32_t max, int32_t val);

static void keyboard_event_cb(lv_event_t* e);
static void wisun_connect_runner();

/*
 * Add the given message in the log table
 */
void add_log(char *message) {
    size_t row_count = lv_table_get_row_cnt(log_table);

    lv_table_set_cell_value(log_table, row_count, 0, "No time");
    lv_table_set_cell_value(log_table, row_count, 1, message);

    log_count++;
    update_log_tab_title();
}

void start_display() {
    // start uptime timer
    t.start();

    // initialize display through LVGL
    lv_init();
    lv_port_disp_init();
    lv_port_indev_init();

    // attack LVGL ticker callback
    lvgl_ticker.attach(callback(&lvgl_ticker_cb), TICKER_TIME);
    status_ticker.attach(callback(&status_ticker_cb), TICKER_STATUS_TIME);

    // set default LCD brightness
    BSP_LCD_SetBrightness(LCD_BRIGHTNESS_DEFAULT);

    displayButton.mode(PullUp);
    display_timer.start();
    queue = mbed_event_queue();
}

void build_interface() {
    // initialize custom style
    lv_style_init(&style);
    lv_style_set_text_font(&style, &symbol_custom);

    /* Tabs */
    tabview = lv_tabview_create(lv_scr_act(), LV_DIR_TOP, 50);
    lv_obj_add_style(tabview, &style, 0);

    lv_obj_t* tab_general = lv_tabview_add_tab(tabview, LV_SYMBOL_HOME);
    lv_obj_t* tab_msg = lv_tabview_add_tab(tabview, MY_SYMBOL_COMMENTS);
    lv_obj_t* tab_log = lv_tabview_add_tab(tabview, LV_SYMBOL_BELL);
    lv_obj_add_event_cb(tab_log, log_tab_cb, LV_EVENT_ALL, tab_log);
    lv_obj_t* tab_setting = lv_tabview_add_tab(tabview, LV_SYMBOL_SETTINGS);

    /* Tab > General */

    label_mesh_status = lv_label_create(tab_general);

    lv_style_t style2;
    lv_style_init(&style2);
    lv_style_set_text_font(&style2, &lv_font_montserrat_40);
    lv_label_set_text(label_mesh_status, "State: waiting for mesh start");
    lv_label_set_long_mode(label_mesh_status, LV_LABEL_LONG_WRAP); /*Break the long lines*/
    lv_label_set_recolor(label_mesh_status, true); /*Enable re-coloring by commands in the text*/
    lv_obj_set_width(label_mesh_status, 750);
    uptime_label = lv_label_create(tab_general);
    lv_obj_align(uptime_label, LV_ALIGN_TOP_LEFT, 0, 40);
    lv_obj_add_style(uptime_label, &style2, 0);
    lv_obj_add_style(label_mesh_status, &style2, 0);

    btn_mesh_connect = lv_btn_create(tab_general);
    lv_obj_add_event_cb(btn_mesh_connect, wisun_start_cb, LV_EVENT_CLICKED, NULL);
    lv_obj_align(btn_mesh_connect, LV_ALIGN_CENTER, 0, 0);
    lv_obj_t* label = lv_label_create(btn_mesh_connect);
    lv_label_set_text(label, "Join mesh network");
    lv_obj_center(label);
    lv_obj_set_height(btn_mesh_connect, 60);

    lv_timer_create(uptime_cb, 1000, NULL);

    /* Tab > Messaging */

    lv_obj_t* cont;
    lv_obj_t* kb = lv_keyboard_create(tab_msg);
    lv_obj_t* ta;
    ta = lv_textarea_create(tab_msg);
    lv_textarea_set_max_length(ta, MESSAGE_MAX_LENGTH);
    lv_obj_align(ta, LV_ALIGN_TOP_LEFT, 10, 10);
    lv_obj_add_event_cb(ta, keyboard_event_cb, LV_EVENT_ALL, kb);

    label = create_text(tab_msg, LV_SYMBOL_LIST, "Topic", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_obj_align(label, LV_ALIGN_TOP_LEFT, 10, 100);
    topic_dropdown = lv_dropdown_create(tab_msg);
    lv_dropdown_set_options(topic_dropdown, topics);
    lv_obj_align(topic_dropdown, LV_ALIGN_TOP_LEFT, 130, 100);
    lv_obj_set_width(topic_dropdown, 300);

    lv_textarea_set_placeholder_text(ta, "Enter your message");
    lv_obj_set_size(ta, 750, 80);
    lv_keyboard_set_textarea(kb, ta);

    /* Tab > Log */
    log_table = lv_table_create(tab_log);

    /*Fill the first column*/
    lv_table_set_cell_value(log_table, 0, 0, "Time");

    /*Fill the second column*/
    lv_table_set_cell_value(log_table, 0, 1, "Message");

    /*Set a smaller height to the table. It'll make it scrollable*/
    lv_obj_set_width(log_table, MY_DISP_HOR_RES - 50);
    lv_obj_set_height(log_table, MY_DISP_VER_RES - 80);
    lv_table_set_col_width(log_table, 0, 280);
    lv_table_set_col_width(log_table, 1, 460);
    lv_obj_center(log_table);

    /* Tab > Settings */

    /* Menu */

    lv_obj_t* menu = lv_menu_create(tab_setting);

    lv_color_t bg_color = lv_obj_get_style_bg_color(menu, 0);
    if (lv_color_brightness(bg_color) > 127) {
        lv_obj_set_style_bg_color(menu, lv_color_darken(lv_obj_get_style_bg_color(menu, 0), 10), 0);
    } else {
        lv_obj_set_style_bg_color(menu, lv_color_darken(lv_obj_get_style_bg_color(menu, 0), 50), 0);
    }
    lv_menu_set_mode_root_back_btn(menu, LV_MENU_ROOT_BACK_BTN_DISABLED);
    //lv_obj_add_event_cb(menu, back_event_handler, LV_EVENT_CLICKED, menu);
    lv_obj_set_size(menu, lv_disp_get_hor_res(NULL)-40, lv_disp_get_ver_res(NULL)-60);
    lv_obj_align(menu, LV_ALIGN_BOTTOM_MID, 0, 20);

    /* Submenus */

    lv_obj_t* btn;
    lv_obj_t* section;
    lv_obj_t* section_bis;

    /* Tab > Settings > Time */

    lv_obj_t* sub_time_page = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_time_page, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);
    lv_menu_separator_create(sub_time_page);
    section = lv_menu_section_create(sub_time_page);

    // Clock label
    clock_label = create_text(section, NULL, "", LV_MENU_ITEM_BUILDER_VARIANT_1);

    /* Tab > Settings > Time > Change date & time */
    lv_obj_t* sub_time_set = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_time_set, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);

    section_bis = lv_menu_section_create(sub_time_set);
    lv_label_set_long_mode(
        lv_obj_get_child(create_text(section_bis, NULL, "Please set the time and date, then press the button at the bottom of this page.", LV_MENU_ITEM_BUILDER_VARIANT_1), 0),
        LV_LABEL_LONG_WRAP
    );

    new_datetime_label = create_text(section_bis, NULL, "", LV_MENU_ITEM_BUILDER_VARIANT_1);
    //update_new_datetime_label();

    /* Tab > Settings > Display */

    lv_obj_t* sub_display_page = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_display_page, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);
    lv_menu_separator_create(sub_display_page);
    section = lv_menu_section_create(sub_display_page);
    lv_obj_t* lcd_slider = create_slider(section, MY_SYMBOL_LIGHTBULB, "Brightness", LCD_BRIGHTNESS_MIN, LCD_BRIGHTNESS_MAX, LCD_BRIGHTNESS_DEFAULT);
    lv_obj_add_event_cb(lv_obj_get_child(lcd_slider, -1), brightness_cb, LV_EVENT_VALUE_CHANGED, menu);

    /* Tab > Settings > Network */

    lv_obj_t* sub_network_page = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_network_page, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);
    lv_menu_separator_create(sub_network_page);
    section = lv_menu_section_create(sub_network_page);

    /* Tab > Settings > Network > MQTT */
    lv_obj_t* sub_network_mqtt = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_network_mqtt, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);

    section_bis = lv_menu_section_create(sub_network_mqtt);

    create_text(section_bis, NULL, "Client", LV_MENU_ITEM_BUILDER_VARIANT_1);
    label_mqtt_client = lv_obj_get_child(create_text(section_bis, NULL, "Status: #ff0000 disconnected#", LV_MENU_ITEM_BUILDER_VARIANT_1), -1);
    lv_label_set_recolor(label_mqtt_client, true);

    section_bis = lv_menu_section_create(sub_network_mqtt);

    create_text(section_bis, NULL, "Broker", LV_MENU_ITEM_BUILDER_VARIANT_1);

    cont = lv_menu_cont_create(section_bis);
    label_broker_ip = lv_label_create(cont);
    lv_label_set_text(label_broker_ip, "IP: unknown");

    cont = lv_menu_cont_create(section_bis);
    label_broker_port = lv_label_create(cont);
    lv_label_set_text(label_broker_port, "Port: unknown");

    cont = lv_menu_cont_create(section_bis);
    btn_broker_connect = lv_btn_create(cont);
    lv_obj_add_event_cb(btn_broker_connect, broker_connect_cb, LV_EVENT_CLICKED, NULL);
    label = lv_label_create(btn_broker_connect);
    hide_obj(btn_broker_connect); // hide connect until sync
    lv_label_set_text(label, "Connect");
    lv_obj_center(label);

    btn_broker_disconnect = lv_btn_create(cont);
    lv_obj_add_event_cb(btn_broker_disconnect, broker_disconnect_cb, LV_EVENT_CLICKED, NULL);
    label = lv_label_create(btn_broker_disconnect);
    lv_label_set_text(label, "Disconnect");
    hide_obj(btn_broker_disconnect); // hide disconnect initially
    lv_obj_center(label);

    cont = create_text(section, MY_SYMBOL_SERVER, "MQTT", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_network_mqtt);

    /* Tab > Settings > Network > Mesh */
    lv_obj_t* sub_network_mesh = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_network_mesh, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);

    section_bis = lv_menu_section_create(sub_network_mesh);

    create_text(section_bis, NULL, "Wi-SUN", LV_MENU_ITEM_BUILDER_VARIANT_1);
    label_mesh0 = lv_obj_get_child(create_text(section_bis, NULL, "Status: #ff0000 down#", LV_MENU_ITEM_BUILDER_VARIANT_1), -1);
    lv_label_set_recolor(label_mesh0, true);
    lv_obj_align(section_bis, LV_ALIGN_CENTER, 0, 40);

    cont = lv_menu_cont_create(section_bis);
    btn_mesh_disconnect = lv_btn_create(cont);
    lv_obj_add_event_cb(btn_mesh_disconnect, wisun_disconnect_cb, LV_EVENT_CLICKED, NULL);
    label = lv_label_create(btn_mesh_disconnect);
    lv_label_set_text(label, "Disconnect");
    lv_obj_center(label);
    hide_obj(btn_mesh_disconnect);

    cont = create_text(section, MY_SYMBOL_SITEMAP, "Mesh", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_network_mesh);

    /* Tab > Settings > Network > Sync */
    lv_obj_t* sub_network_sync = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_network_sync, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);

    section_bis = lv_menu_section_create(sub_network_sync);

    sync_label = lv_obj_get_child(create_text(section_bis, NULL, "Sync service: #ff0000 stopped#", LV_MENU_ITEM_BUILDER_VARIANT_1), 0);
    lv_label_set_recolor(sync_label, true);

    cont = lv_menu_cont_create(section_bis);
    btn_sync = lv_btn_create(cont);
    lv_obj_add_event_cb(btn_sync, sync_cb, LV_EVENT_CLICKED, NULL);
    label = lv_label_create(btn_sync);
    lv_label_set_text(label, "Start sync process");
    hide_obj(btn_sync);

    lv_obj_align(section_bis, LV_ALIGN_CENTER, 0, 40);

    cont = create_text(section, MY_SYMBOL_SATELLITE_DISH, "Sync", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_network_sync);

    /* Tab > Settings > System */
    lv_obj_t* sub_system_page = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_system_page, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);
    lv_menu_separator_create(sub_system_page);
    section = lv_menu_section_create(sub_system_page);
    label_mem = create_text(section, NULL, "%d", LV_MENU_ITEM_BUILDER_VARIANT_1);

    /* Tab > Settings > About */

    lv_obj_t* sub_software_info_page = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_software_info_page, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);
    section = lv_menu_section_create(sub_software_info_page);
    char version[29];
    snprintf(version, 29, "%s v%d.%d.%d", PROGRAM_NAME, VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);
    char release[25];
    sprintf(release, "Release date: %s", RELEASE_DATE);
    create_text(section, NULL, version, LV_MENU_ITEM_BUILDER_VARIANT_1);
    create_text(section, NULL, release, LV_MENU_ITEM_BUILDER_VARIANT_1);

    lv_obj_t* sub_about_page = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(sub_about_page, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);
    lv_menu_separator_create(sub_about_page);
    section = lv_menu_section_create(sub_about_page);
    cont = create_text(section, NULL, "Software information", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_software_info_page);

    /*Create a root page*/
    lv_obj_t* root_page = lv_menu_page_create(menu, NULL);
    lv_obj_set_style_pad_hor(root_page, lv_obj_get_style_pad_left(lv_menu_get_main_header(menu), 0), 0);
    section = lv_menu_section_create(root_page);

    /* Create menus */
    cont = create_text(section, MY_SYMBOL_CLOCK, "Time", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_time_page);
    cont = create_text(section, MY_SYMBOL_DESKTOP, "Display", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_display_page);
    cont = create_text(section, MY_SYMBOL_ETHERNET, "Network", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_network_page);
    cont = create_text(section, MY_SYMBOL_SERVER, "System", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_system_page);


    create_text(root_page, NULL, "Others", LV_MENU_ITEM_BUILDER_VARIANT_1);
    section = lv_menu_section_create(root_page);
    cont = create_text(section, NULL, "About", LV_MENU_ITEM_BUILDER_VARIANT_1);
    lv_menu_set_load_page_event(menu, cont, sub_about_page);

    lv_menu_set_sidebar_page(menu, root_page);

    lv_event_send(lv_obj_get_child(lv_obj_get_child(lv_menu_get_cur_sidebar_page(menu), 0), 0), LV_EVENT_CLICKED,
                      NULL);
}

bool display_enabled() {
    return displayEnabled;
}

/**********************
 *   STATIC FUNCTIONS
 **********************/

/*
 * Callback function for lvgl timing.
 * Call lv_tick_inc(x) every x milliseconds in a Timer or Task (x should be between 1 and 10).
 * It is required for the internal timing of LittlevGL.
 */
static void lvgl_ticker_cb(){
    if (displayEnabled) {
        lv_tick_inc(LVGL_TICK);
    }

    if (displayButton.read()) {
        queue->call(lcd_on_off_func);
    }
}

/* LCD & display functions */

/*
 * Handles the LCD on/off button events.
 */
static void lcd_on_off_func() {
    auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(display_timer.elapsed_time()).count();

    if (diff > 1000) {
        display_timer.stop();
        display_timer.reset();

        if (displayEnabled) {
            BSP_LCD_SetBrightness(0);
            BSP_LCD_DisplayOff();
        } else {
            BSP_LCD_DisplayOn();
            BSP_LCD_SetBrightness(last_brightness);
        }

        displayEnabled = !displayEnabled;
        display_timer.start();
    }
}

/*
 * Sets the LCD brightness according to the slider value
 */
static void brightness_cb(lv_event_t * e) {
    last_brightness = lv_slider_get_value(lv_event_get_target(e));
    BSP_LCD_SetBrightness(last_brightness);
}

/*
 * Refreshes various status labels
 */
static void status_ticker_cb() {
    update_mesh_info;

    update_mqtt_info();

    queue->call(update_memory_info);
}

static void update_mesh_info() {
    if (is_mesh_connected()) {
        auto mesh = get_mesh_iface();
        auto status = mesh->get_connection_status();

        switch (status) {
            case NSAPI_STATUS_DISCONNECTED:
                lv_label_set_text(label_mesh0, "Status: #ff0000 down#");
                break;
            case NSAPI_STATUS_CONNECTING:
                lv_label_set_text(label_mesh0, "Status: #0000ff connecting#");
                break;
            case NSAPI_STATUS_ERROR_UNSUPPORTED:
                break;
            case NSAPI_STATUS_LOCAL_UP:
            case NSAPI_STATUS_GLOBAL_UP:
                char iface_name[128] = {0};
                auto ret = mesh->get_interface_name(iface_name);

                char text[34 + 3 + 128 + 1];
                sprintf(text, "Status: #009e00 up#\nAddress: %s", get_mesh_address());

                lv_label_set_text(label_mesh0, text);
            break;
        }
    }
}

static void update_memory_info() {
    mbed_stats_sys_get(&stats);
    mbed_stats_heap_get(&heap_info);
    lv_label_set_text_fmt(lv_obj_get_child(label_mem, 0), "Bytes allocated currently: %ld", heap_info.current_size);
}

static void update_mqtt_info() {
    auto mqtt_status = is_client_connected();
    switch (mqtt_status) {
        case NSAPI_ERROR_OK:
            lv_label_set_text(label_mqtt_client, "Status: #009e00 connected#");
            if (mqtt_status != last_mqtt_status) {
                add_log("MQTT: connected to broker");
            }
            break;
        case NSAPI_ERROR_CONNECTION_LOST:
            lv_label_set_text(label_mqtt_client, "Status: #ff0000 connection lost#");
            if (mqtt_status != last_mqtt_status) {
                add_log("MQTT: lost connection to broker");
            }
            break;
        case NSAPI_ERROR_NO_CONNECTION:
            lv_label_set_text(label_mqtt_client, "Status: #ff0000 disconnected#");
            if (mqtt_status != last_mqtt_status) {
                add_log("MQTT: disconnected from broker");
            }
            break;
    }
    last_mqtt_status = mqtt_status;

    char message[128];
    bool ip_port_ready = false;

    char ip[128];
    auto ret = get_broker_ip(ip);
    if (ret == 0) {
        sprintf(message, "IP: %s", ip);
        lv_label_set_text(label_broker_ip, message);
        ip_port_ready = true;
    }

    auto port = get_broker_port();
    if (port) {
        sprintf(message, "Port: %u", port);
        lv_label_set_text(label_broker_port, message);
        ip_port_ready &= true;
    }

    if (ip_port_ready && is_client_connected() != NSAPI_ERROR_OK) {
        show_obj(btn_broker_connect);
    }
}

/*
 * Clear the log count when tab is opened
 */
static void log_tab_cb(lv_event_t* e) {
    if (lv_tabview_get_tab_act(tabview) != 2) return;

    log_count = 0;
    update_log_tab_title();
}

/*
 * Update the log tab title with the current log count (if nonzero)
 */
static void update_log_tab_title() {
    char buffer[32];
    sprintf(buffer, "%s", LV_SYMBOL_BELL);
    if (log_count > 0) {
        sprintf(buffer + 3, " %d", log_count);
    }
    lv_tabview_rename_tab(tabview, 2, buffer);
}

/*
 * Start the broker connection
 */
static void broker_connect_cb(lv_event_t* e) {
    hide_obj(btn_broker_connect);
    printf("\n");
    queue->call(connect_to_broker);
}

static void connect_to_broker() {
    printf("\n");
    auto ret = open_socket();
    if (ret != NSAPI_ERROR_OK) {
        popup = lv_msgbox_create(NULL, "Error", "Mesh interface is down.", {NULL}, true);
        lv_obj_set_width(popup, 600);
        lv_obj_center(popup);
        close_socket();
        show_obj(btn_broker_connect);
        return;
    }

    ret = connect_socket();
    if (ret != NSAPI_ERROR_OK) {
        popup = lv_msgbox_create(NULL, "Error", "Broker unreachable. Check IP settings and try again later.", {NULL}, true);
        lv_obj_center(popup);
        close_socket();
        show_obj(btn_broker_connect);
        return;
    }

    ret = connect_client();
    if (ret != NSAPI_ERROR_OK) {
        popup = lv_msgbox_create(NULL, "Error", "Connection failed, check broker logs.", {NULL}, true);
        lv_obj_center(popup);
        close_socket();
        show_obj(btn_broker_connect);
        return;
    }

    popup = lv_msgbox_create(NULL, "Success", "Connected to broker", {NULL}, true);
    lv_obj_center(popup);

    show_obj(btn_broker_disconnect);
}

/*
 * End the broker connection
 */
static void broker_disconnect_cb(lv_event_t* e) {
    disconnect_client();

    // swap buttons
    hide_obj(btn_broker_disconnect);
    show_obj(btn_broker_connect);

    popup = lv_msgbox_create(NULL, "Success", "Disconnected from broker", {NULL}, true);
    lv_obj_center(popup);
}

/*
 * Start the Wi-SUN stack
 */
static void wisun_start_cb(lv_event_t* e) {
    if (init_mesh_iface()) {
        popup = lv_msgbox_create(NULL, "Error", "Mesh interface unavailable", {NULL}, true);
        lv_obj_center(popup);
    }

    lv_label_set_text(label_mesh_status, "State: #0000ff connecting#");
    thread.start(wisun_connect_runner);
}

/*
 * Disconnect from the Wi-SUN network
 */
static void wisun_disconnect_cb(lv_event_t* e) {
    hide_obj(btn_mesh_disconnect);
    lv_label_set_text(label_mesh_status, "State: #0000ff disconnecting#");

    auto mesh = get_mesh_iface();
    mesh->disconnect();
    add_log("Mesh: disconnected");
    lv_label_set_text(label_mesh_status, "State: #ff0000 disconnected#");

    show_obj(btn_mesh_connect);
    hide_obj(btn_sync);
}


/*
 * Wi-SUN connection runner, within EventQueue
 */
static void wisun_connect_runner() {
    // long blocking call during Wi-SUN join procedure
    add_log("Mesh: connecting");
    hide_obj(btn_mesh_connect);
    int error = connect_to_mesh();
    if (error) {
        lv_label_set_text(label_mesh_status, "State: #ff0000 error#");

        popup = lv_msgbox_create(NULL, "Error", "Could not join mesh network.", {NULL}, true);
        lv_obj_center(popup);

        add_log("Error while joining mesh network");
        show_obj(btn_mesh_connect);
        return;
    }

    // wait until an IP is delivered to the node
    auto mesh = get_mesh_iface();
    SocketAddress sockAddr;
    while (mesh->get_ip_address(&sockAddr) != NSAPI_ERROR_OK) {
        wait_us(1000000);
    }
    set_mesh_address((char*) sockAddr.get_ip_address());

    // update status label
    lv_label_set_text(label_mesh_status, "State: #009e00 connected#");
    char message[128];
    sprintf(message, "Mesh: connected with IP %s", sockAddr.get_ip_address());
    add_log(message);
    show_obj(btn_mesh_disconnect);

    // init sync socket
    auto ret = init_sync();
    if (ret != NSAPI_ERROR_OK) {
        popup = lv_msgbox_create(NULL, "Error", "Could not initialize sync process.", {NULL}, true);
        lv_obj_center(popup);
        return;
    }

    show_obj(btn_sync);
}

static void sync_cb(lv_event_t* e) {
    start_sync_listener();
    lv_label_set_text(sync_label, "Sync service: #009e00 started#");
    hide_obj(btn_sync);
}

/*
 * Updates the uptime and clock labels
 */
static void uptime_cb(lv_timer_t* timer) {
    auto us = t.elapsed_time();
    lv_label_set_text_fmt(
        uptime_label,
        "Uptime: %0*llu:%0*llu:%0*llu",
        2, std::chrono::duration_cast<std::chrono::hours>(us).count(),
        2, std::chrono::duration_cast<std::chrono::minutes>(us).count() % 60,
        2, std::chrono::duration_cast<std::chrono::seconds>(us).count() % 60
    );

    // auto ret = get_time(&rtc_time);
    // ret |= get_calendar(&rtc_calendar);
    // if (ret == 0) {
    //     lv_label_set_text_fmt(
    //         lv_obj_get_child(clock_label, 0),
    //         "Current RTC clock: %0*lu.%0*lu.20%0*lu %0*lu:%0*lu:%0*lu",
    //         2, rtc_calendar.date,
    //         2, rtc_calendar.month,
    //         2, rtc_calendar.year,
    //         2, rtc_time.hours,
    //         2, rtc_time.minutes,
    //         2, rtc_time.seconds
    //     );
    // }
}

/*
 * Handle text input for MQTT messaging
 */
static void keyboard_event_cb(lv_event_t * e) {
    lv_obj_t * ta = lv_event_get_target(e);

    if (lv_event_get_code(e) == LV_EVENT_READY) {
        auto client_status = is_client_connected();

        if (client_status == NSAPI_ERROR_NO_CONNECTION or client_status == NSAPI_ERROR_CONNECTION_LOST) {
            popup = lv_msgbox_create(NULL, "Error", "Disconnected from MQTT broker.", {NULL}, true);
            lv_obj_center(popup);
        } else {
            char* message = (char*) lv_textarea_get_text(ta);

            if (strlen(message) == 0) {
                popup = lv_msgbox_create(NULL, "Error", "Your message is empty.", {NULL}, true);
                lv_obj_center(popup);
            } else {
                char topic[64];
                lv_dropdown_get_selected_str(topic_dropdown, topic, sizeof(topic) / sizeof(char));
                publish(topic, message);
            }
        }
    }
}

/* Helper functions */

static lv_obj_t * create_text(lv_obj_t * parent, const char * icon, const char * txt, lv_menu_builder_variant_t builder_variant) {
    lv_obj_t * obj = lv_menu_cont_create(parent);

    lv_obj_t * img = NULL;
    lv_obj_t * label = NULL;

    if(icon) {
        img = lv_img_create(obj);
        lv_img_set_src(img, icon);
    }

    if(txt) {
        label = lv_label_create(obj);
        lv_label_set_text(label, txt);
        lv_label_set_long_mode(label, LV_LABEL_LONG_SCROLL_CIRCULAR);
        lv_obj_set_flex_grow(label, 1);
    }

    if(builder_variant == LV_MENU_ITEM_BUILDER_VARIANT_2 && icon && txt) {
        lv_obj_add_flag(img, LV_OBJ_FLAG_FLEX_IN_NEW_TRACK);
        lv_obj_swap(img, label);
    }

    return obj;
}

static lv_obj_t * create_slider(lv_obj_t * parent, const char * icon, const char * txt, int32_t min, int32_t max, int32_t val) {
    lv_obj_t * obj = create_text(parent, icon, txt, LV_MENU_ITEM_BUILDER_VARIANT_2);

    lv_obj_t * slider = lv_slider_create(obj);
    lv_obj_set_flex_grow(slider, 1);
    lv_slider_set_range(slider, min, max);
    lv_slider_set_value(slider, val, LV_ANIM_OFF);

    if(icon == NULL) {
        lv_obj_add_flag(slider, LV_OBJ_FLAG_FLEX_IN_NEW_TRACK);
    }

    return obj;
}

void hide_obj(lv_obj_t* obj) {
    lv_obj_add_flag(obj, LV_OBJ_FLAG_HIDDEN);
}

void show_obj(lv_obj_t* obj) {
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_HIDDEN);
}
