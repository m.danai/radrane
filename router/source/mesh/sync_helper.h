#ifndef SYNC_HELPER_H
#define SYNC_HELPER_H

#include "ds3231.h"
#include "SocketAddress.h"

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct {
    ds3231_time_t time;
    ds3231_calendar_t calendar;
    SocketAddress broker;
} sync_data_t;

#define SYNC_MCAST_GROUP "ff02::2"
#define SYNC_PORT 52414

/*
 * Initialize the UDPv6 socket on the mesh interface so that it is ready
 * to receive multicast traffic on the appropriate group & port
 */
nsapi_error_t init_sync();

/*
 * Starts the sync listener process
 */
nsapi_error_t start_sync();


/*
 *
 */
void start_sync_listener();

#ifdef __cplusplus
}
#endif

#endif /* SYNC_HELPER_H */