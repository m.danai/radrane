#include <stdlib.h>
#include <sys/types.h>
#include "mesh_helper.h"
#include "sync_helper.h"
#include "time_helper.h"
#include "mqtt_helper.h"
#include "display_helper.h"

#include "mbed.h"

static Thread thread;
static EventQueue* queue;
static UDPSocket socket;
static bool socket_ready = false;
static const SocketAddress multicast_group(SYNC_MCAST_GROUP, SYNC_PORT);

static void sync_listener();

nsapi_error_t init_sync() {
    if (socket_ready){
        return NSAPI_ERROR_OK;
    }

    auto ret = socket.open(get_mesh_iface());
    if (ret != NSAPI_ERROR_OK) {
        return ret;
    }

    ret = socket.join_multicast_group(multicast_group);
    ret = socket.bind(SYNC_PORT);

    if (queue == NULL) {
        queue = mbed_event_queue();
    }

    if (ret == NSAPI_ERROR_OK) {
        socket_ready = true;
    }

    return ret;
}

void start_sync_listener() {
    thread.start(sync_listener);
}

static void sync_listener() {
    sync_data_t data;
    SocketAddress sender;
    add_log("Sync process started");

    while (true) {
        auto ret = socket.recvfrom(&sender, &data, sizeof(data));
        if (ret == sizeof(data)) {
            // MQTT port
            u_int16_t old_port = get_broker_port();
            u_int16_t new_port = data.broker.get_port();
            if (old_port != new_port) {
                set_broker_port(new_port);
                char message[32];
                sprintf(message, "Sync: set broker port to %u", new_port);
                add_log(message);
            }

            // MQTT IP
            char ip[128] = {0};
            int ret = get_broker_ip(ip);
            if (ret == 1 or ip[0] == 0) {
                set_broker_ip((char*) data.broker.get_ip_address());
                char message[128];
                sprintf(message, "Sync: set broker IP to %s", data.broker.get_ip_address());
                add_log(message);
            }

            // RTC
            char new_datetime[48];
            format_rtc_date_string(new_datetime, &data.time, &data.calendar);
            char message[128];
            sprintf(message, "Sync: set time to %s", new_datetime);
            add_log(message);
        }
    }
}
