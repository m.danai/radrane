#ifndef MESH_HELPER_H
#define MESH_HELPER_H

#include "NanostackInterface.h"

#ifdef __cplusplus
extern "C"
{
#endif

bool init_mesh_iface();
nsapi_error_t connect_to_mesh();
bool is_mesh_connected();
MeshInterface* get_mesh_iface();
void set_mesh_address(char* ip);
const char* get_mesh_address();

#ifdef __cplusplus
}
#endif

#endif /* MESH_HELPER_H */
