#include "mesh_helper.h"

#include "mbed.h"

static MeshInterface* mesh;
static SocketAddress address;
static bool connected = false;

bool init_mesh_iface() {
    if (mesh == NULL) {
        mesh = MeshInterface::get_default_instance();
    }

    return mesh == NULL;
}

nsapi_error_t connect_to_mesh() {
    auto ret = mesh->connect();
    if (ret == NSAPI_ERROR_OK) {
        connected = true;
    }
    return ret;
}

bool is_mesh_connected() {
    return connected;
}

MeshInterface* get_mesh_iface() {
    return mesh;
}

void set_mesh_address(char* ip) {
    address.set_ip_address(ip);
}

const char* get_mesh_address() {
    return address.get_ip_address();
}
