#ifndef TIME_HELPER_H
#define TIME_HELPER_H

#include "mbed.h"
#include "ds3231.h"

#ifdef __cplusplus
extern "C"
{
#endif

void init_rtc(PinName sda, PinName scl);
uint16_t get_calendar(ds3231_calendar_t* cal);
uint16_t get_time(ds3231_time_t* time);
uint16_t set_datetime(ds3231_calendar_t cal, ds3231_time_t time);
void format_rtc_date_string(char* buffer, ds3231_time_t* time, ds3231_calendar_t* cal);
size_t get_week_day(uint day, uint month, uint year);

#ifdef __cplusplus
}
#endif

#endif /* TIME_HELPER_H */