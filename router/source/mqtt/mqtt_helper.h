#ifndef MQTT_HELPER_H
#define MQTT_HELPER_H

#include "EthInterface.h"
#include <MQTTClientMbedOs.h>
#include "MQTTClient.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define TOPIC_TEST "test"
#define TOPIC_EMERGENCY "Emergency"

#define KEEPALIVE_TIME 5s

nsapi_error_t close_socket();
nsapi_error_t open_socket();
nsapi_error_t connect_socket();
int connect_client();
void disconnect_client();
void publish(char* topic, char* message, MQTT::QoS qos = MQTT::QOS1, bool dup = false);
nsapi_error_t is_client_connected();
void reset_client();
void message_cb(MQTT::MessageData& msgData);
u_int16_t get_broker_port();
void set_broker_port(u_int16_t port);
int get_broker_ip(char* dest);
void set_broker_ip(char* ip);

#ifdef __cplusplus
}
#endif

#endif /* MQTT_HELPER_H */
