#include "mbed.h"

#include "display/display_helper.h"

#include "mbed-trace/mbed_trace.h"

void trace_printer(const char* str) {
    printf("%s\n", str);
}

static Mutex SerialOutMutex;

void serial_out_mutex_wait() {
    SerialOutMutex.lock();
}

void serial_out_mutex_release() {
    SerialOutMutex.unlock();
}

int main () {
    mbed_trace_init();
    mbed_trace_print_function_set(trace_printer);
    mbed_trace_mutex_wait_function_set( serial_out_mutex_wait );
    mbed_trace_mutex_release_function_set( serial_out_mutex_release );

    start_display();
    build_interface();

    while (true){
        if (display_enabled()) {
            lv_task_handler();
        }
        //Call lv_task_handler() periodically every few milliseconds.
        //It will redraw the screen if required, handle input devices etc.
        thread_sleep_for(LVGL_TICK);
    }
}