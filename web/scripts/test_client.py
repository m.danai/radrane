import datetime as dt
import paho.mqtt.client as mqtt

BROKER_IP = '2001:db8::3'
BROKER_PORT = 1883
TOPICS = ['test', 'A', 'Emergency']

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code {}".format(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    for topic in TOPICS:
        client.subscribe(topic, 2)

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print("[{}] {}: {}".format(
        dt.datetime.now().strftime('%c'),
        msg.topic,
        msg.payload.decode('utf8')
    ))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(BROKER_IP, BROKER_PORT, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.

client.publish("Emergency", "test", 2)

client.loop_forever()
