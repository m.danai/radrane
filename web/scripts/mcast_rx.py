import socket
import struct
import ipaddress

# define multicast constants
MCAST_ADDR = 'ff02::2'
MCAST_PORT = 52414
INTERFACE_NAME = 'enp2s0f0'

# get the index of the named interface
interface_index = socket.if_nametoindex(INTERFACE_NAME)

# create the JOIN option data
mc_addr = ipaddress.IPv6Address(MCAST_ADDR)
join_data = struct.pack('16sI', mc_addr.packed, interface_index)

# create an UDPv6 socket
sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)

# join the multicast group
sock.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_JOIN_GROUP, join_data)

# bind to named interface only to avoid sending trafic on the wrong interface
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, INTERFACE_NAME.encode('utf8'))

# bind the socket to the RADRANE multicast port
sock.bind(('', MCAST_PORT))

# wait and print data
while True:
    data, sender = sock.recvfrom(150)
    print('[{}] {}'.format(sender, data))