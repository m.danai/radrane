from setuptools import setup

setup(
    name='RADRANE',
    version='0.1.0',
    packages=['radrane'],
    include_package_data=True,
    install_requires=[
        'flask',
        'wtforms',
        'flask-wtf',
    ],
    extras_require={
        'dev': [
        ],
        'testing': [
            'pytest'
        ],
        'prod': [
            'gunicorn',
        ],
    }
)
