import os
import random
import string
from radrane.security import gen_rand_string

SECRET_KEY_LEN = 256

class Config:
    SECRET_KEY = ''.join(
        random.SystemRandom().choice(
            string.ascii_letters + string.digits + string.punctuation
        ) for _ in range(SECRET_KEY_LEN)
    )
    WTF_CSRF_ENABLED = True

class DevConfig(Config):
    DEBUG = True
    FLASK_DEBUG = True

class StagingConfig(Config):
    pass

class ProdConfig(Config):
    pass