import random
import string

API_ID_LEN = 13

def gen_rand_string(prefix=None):
    return (prefix if prefix else '') + ''.join(
        random.SystemRandom().choice(
            string.ascii_letters + string.digits
        ) for _ in range(API_ID_LEN)
    )
