import subprocess, shlex

from flask import flash, redirect, render_template, request, url_for, make_response

from radrane import APP
from radrane.forms import MessageForm

CMD = './mqtt-sn-tools/mqtt-sn-pub -m "{message}" -t "{topic}" -p {port} -h {host}%{iface}'
GATEWAY_PORT = 10000
GATEWAY_INTERFACE = 'enp2s0f0'
CLIENT_COUNT = 1

@APP.route('/', methods=['GET', 'POST'])
def index():
    form = MessageForm()
    res = None

    form.recipient.choices.append(('fe80::b8fe:170f:c557:9292', 'This machine'))
    form.recipient.choices.append(('ff02::1', 'Broadcast'))

    form.topic.choices.append(('emergency', 'Urgence'))
    form.topic.choices.append(('debug', 'Debug'))

    if request.method == 'POST':
        if form.validate():
            for i in range(CLIENT_COUNT):
                run = subprocess.Popen(
                    shlex.split(CMD.format(
                        message=form.message.data + f' {i}',
                        topic=form.topic.data,
                        port=GATEWAY_PORT,
                        host=form.recipient.data,
                        iface=GATEWAY_INTERFACE
                    )),
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                )

                for line in run.stdout:
                    print(line)
                for line in run.stderr:
                    print(line)

                #run.wait()

                #res = run.returncode


    return render_template(
        'index.html',
        pagename='index',
        form=form,
        res=res
    )
