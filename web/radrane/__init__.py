# -*- coding: utf-8 -*-
from flask import Flask

from .config import ProdConfig, StagingConfig, DevConfig

# launching app with the specified config
APP = Flask(__name__)
APP.config.from_object(DevConfig)

# getting views
from radrane import views
