from wtforms.fields import StringField, PasswordField, BooleanField, IntegerField, SelectField, FileField, TextAreaField, TelField, DateField
from wtforms.validators import DataRequired, Email, Length, EqualTo, InputRequired, NumberRange
from flask_wtf import FlaskForm

class MessageForm(FlaskForm):
    recipient = SelectField('Destinataire',
        choices=[],
        validators=[
            DataRequired(message='Destinataire requis')
        ]
    )
    topic = SelectField('Sujet',
        choices=[],
        validators=[
            DataRequired(message='Destinataire requis')
        ]
    )
    message = TextAreaField('Message', validators=[
        DataRequired(message='Message requis'),
    ])
